package com.iranplays.lingotv;

class Mohammad {
    private static Mohammad instance = null;

    public static Mohammad getInstanceUsingDoubleLocking() {
        if (instance == null) {
            synchronized (Mohammad.class) {
                if (instance == null) {
                    instance = new Mohammad();
                }
            }
        }
        return instance;
    }


    private Mohammad() {
    }
}
