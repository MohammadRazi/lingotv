package com.iranplays.lingotv;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.iranplays.lingotv.ui.page.PlayerActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class MainActivityA extends AppCompatActivity {
    boolean isDark;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        /* get theme */
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            isDark = true;
            setTheme(R.style.DarkTheme);
        } else
            setTheme(R.style.LightTheme);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnStart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDark)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

                startActivity(new Intent(getApplicationContext(), MainActivityA.class));
                finish();
            }
        });

        findViewById(R.id.ivProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PlayerActivity.class));
            }
        });


        findViewById(R.id.ivShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TestDialog.Companion.newInstance().showDialog(getSupportFragmentManager(), "1");
            }
        });
    }

}
