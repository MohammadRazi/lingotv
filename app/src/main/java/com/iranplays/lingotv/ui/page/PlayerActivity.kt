package com.iranplays.lingotv.ui.page

import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.CountDownTimer
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.basearchitecture.base.BestUtility.LogApp
import com.example.basearchitecture.base.EventBusModel
import com.example.basearchitecture.base.baseMvp.BaseActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.iranplays.lingotv.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_player.*
import java.util.*

class PlayerActivity : BaseActivity() {
    override val contentViewId: Int = R.layout.activity_player
    private lateinit var player: SimpleExoPlayer
    private var isChoiceLeftTrue: Boolean = false
    private var canSetScore: Boolean = true
    private var isTimeOver: Boolean = false
    private var isPlayed: Boolean = false
    private lateinit var timer: CountDownTimer
    private val timerSeconds: Long = 30

    enum class ScreenMode {
        Play,
        WithOrWithoutSub,
        ChooseWithSub,
        ChooseWithoutSub,
        TimeOver
    }

    //region base methodes
    override fun onEvent(event: EventBusModel) {
    }

    override fun initBeforeView() {
        setTheme(R.style.DarkTheme)
    }

    override fun initNavigation() {
    }

    override fun initToolbar() {
    }

    override fun initViews() {
        onSuccess()
        llHome.setOnClickListener {
            finish()
            onBackPressed()
        }
        setScreenMode(ScreenMode.Play)
        rightAnswer.setOnClickListener { }
        leftAnswer.setOnClickListener { setScreenMode(ScreenMode.ChooseWithSub) }
        timer = object : CountDownTimer((timerSeconds * 1000), (1 * 1000)) {
            override fun onFinish() {
                afterChoose()

                if (isTimeOver) {
                    setScreenMode(ScreenMode.TimeOver)
                }

            }

            override fun onTick(millisUntilFinished: Long) {
                bPlay.text = (millisUntilFinished / 1000).toString()
                val time: Long = 1
                LogApp.e("__time : "+time+ "__millisUntilFinished : "+millisUntilFinished)

                if (millisUntilFinished/1000 == time) {
                    Observable.just(true).delay(1, java.util.concurrent.TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map<Any> {setTextToZero()}
                        .subscribe()
                }
            }

        }
        bReplay.setOnClickListener { player.seekTo(0) }


    }

    private fun afterChoose() {
        bPlay.textSize = resources.getDimension(R.dimen._4ssp)
        bPlay.text = "Continue"
        bPlay.iconPadding = resources.getDimension(R.dimen._10sdp).toInt()
        bPlay.icon = getDrawable(R.drawable.icv_play)
    }

    //set this -->> isTimeOver=true
    fun setTextToZero() {
        bPlay.text = "0"
        isTimeOver=true
    }

    override fun onStart() {
        super.onStart()
        play()
    }

    override fun onPause() {
        super.onPause()
        player.release()

    }
    //endregion

    private fun onSuccess() {
        val rand = Random()
        isChoiceLeftTrue = rand.nextBoolean()
    }

    private fun play() {
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(DefaultBandwidthMeter())
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        player = ExoPlayerFactory.newSimpleInstance(activity, trackSelector)
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING

        videoPlayer.player = player
        videoPlayer.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL;
        videoPlayer.useController = false
        player.playWhenReady = true
        val dataSourceFactory = DefaultDataSourceFactory(
            Objects.requireNonNull(activity),
            Util.getUserAgent(activity, getString(R.string.app_name))
        )

        val mediaSource = ExtractorMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse("https://cdn.voscreen.net/videos/mp4/4f5jgns18du9lz1a2.mp4"))

        player.prepare(mediaSource)

        player.addListener(object : Player.DefaultEventListener() {

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)

                if (playbackState == Player.STATE_BUFFERING) {
                    progressBar.visibility = View.VISIBLE
                    ConstraintLayout.GONE
                }
                if (playbackState == Player.STATE_IDLE) {
                    progressBar.visibility = View.VISIBLE
                }
                if (playbackState == Player.STATE_READY) {
                    progressBar.visibility = View.INVISIBLE
                }
                if (playbackState == Player.STATE_ENDED) {
                    progressBar.visibility = View.INVISIBLE
                    if(!isPlayed)
                    setScreenMode(ScreenMode.WithOrWithoutSub)
                    isPlayed=true

                }

            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                super.onPlayerError(error)
            }
        })

    }

    private fun setScreenMode(mode: PlayerActivity.ScreenMode) {
        bSubtitle.text = "Farsi sample subtitle"
        when (mode) {

            ScreenMode.Play -> {
                bReplay.visibility = LinearLayout.GONE
                bPlay.visibility = LinearLayout.GONE
                llActionDescription.visibility = LinearLayout.GONE
                bSubtitle.visibility = LinearLayout.GONE
                rightAnswer.visibility = LinearLayout.GONE
                leftAnswer.visibility = LinearLayout.GONE
                llScore.visibility = LinearLayout.GONE
            }
            ScreenMode.WithOrWithoutSub -> {
                bPlay.text="30"
                bPlay.iconPadding = resources.getDimension(R.dimen._4sdp).toInt()
                bReplay.visibility = View.VISIBLE
                bPlay.visibility = View.VISIBLE
                bPlay.icon = getDrawable(R.drawable.ic_timer)
                llScore.visibility = LinearLayout.VISIBLE
                leftAnswer.visibility = LinearLayout.VISIBLE
                rightAnswer.visibility = LinearLayout.VISIBLE
                leftAnswer.text = getString(R.string.with_sub)
                rightAnswer.text = getString(R.string.without_sub)

            }

            ScreenMode.ChooseWithSub -> {


                timer.start()

                LogApp.e("chooseWithSub")
                bReplay.visibility = LinearLayout.VISIBLE
                bSubtitle.visibility = TextView.VISIBLE
                llScore.visibility = LinearLayout.VISIBLE
                rightAnswer.visibility = LinearLayout.VISIBLE
                leftAnswer.visibility = LinearLayout.VISIBLE


                if (isChoiceLeftTrue) {
                    // choiceOneBtn.text = result?.subtitle?.choices?.fa?.answer
                    // choiceTwoBtn.text = result?.subtitle?.choices?.fa?.distractor

                    rightAnswer.text = "sample text in persain (false answer)"
                    leftAnswer.text = "sample text in persain (true answer)"

                    rightAnswer.setOnClickListener {
                        if (canSetScore) {

                            rightAnswer.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.redL))
                            rightAnswer.setTextColor(Color.WHITE)

                            addToNegative()
                            // setScore(UserHandler.getScore() - (1 * (timerSeconds - remainTime)))
                            canSetScore = false
                        }
                    }

                    leftAnswer.setOnClickListener {
                        if (canSetScore) {
                            leftAnswer.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greenLL))
                            leftAnswer.setTextColor(Color.WHITE)
                            addToPositive()
                            //setScore(UserHandler.getScore() + (1 * (remainTime)))
                            canSetScore = false
                        }
                    }
                } else {
                    // choiceTwoBtn.text = result?.subtitle?.choices?.fa?.answer
                    // choiceOneBtn.text = result?.subtitle?.choices?.fa?.distractor

                    leftAnswer.text = "sample text in persain (False answer)"
                    rightAnswer.text = "sample text in persain (True answer)"

                    leftAnswer.setOnClickListener {

                        if (canSetScore) {

                            leftAnswer.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.redL))
                            leftAnswer.setTextColor(Color.WHITE)

                            addToNegative()

                            //setScore(UserHandler.getScore() - (1 * (timerSeconds - remainTime)))
                            canSetScore = false
                        }
                    }

                    rightAnswer.setOnClickListener  {
                        if (canSetScore) {
                            rightAnswer.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context,R.color.greenLL))
                            rightAnswer.setTextColor(Color.WHITE)
                        addToPositive()
                        // setScore(UserHandler.getScore() + (1 * (remainTime)))
                        canSetScore = false
                        }
                    }
                }


                // lastScreenMode = ScreenMode.ChooseWithSub
            }
            /*  ScreenMode.ChooseWithoutSub -> {
                  tvTimer.text = timerSeconds.toString()

                  replayBtn.visibility = LinearLayout.VISIBLE
                  llNext.visibility = LinearLayout.GONE
                  tvSubtitle.visibility = TextView.INVISIBLE
                  menuBtn.visibility = TextView.GONE
                  llTimer.visibility = LinearLayout.VISIBLE
                  llScore.visibility = LinearLayout.VISIBLE
                  btnContainerLay.visibility = LinearLayout.VISIBLE
                  ic_timer.start()

                  if (isChoiceOneTrue) {
                      choiceOneBtn.text = result?.subtitle?.choices?.fa?.answer
                      choiceTwoBtn.text = result?.subtitle?.choices?.fa?.distractor

                      choiceTwoBtn.setOnClickListener {
                          if (canSetScore) {
                              addToNegative()
                              setScore(UserHandler.getScore() - (2 * (timerSeconds - remainTime)))
                              canSetScore = false
                          }
                      }

                      choiceOneBtn.setOnClickListener {
                          if (canSetScore) {
                              addToPositive()
                              setScore(UserHandler.getScore() + (2 * (remainTime)))
                              canSetScore = false
                          }
                      }
                  } else {
                      choiceTwoBtn.text = result?.subtitle?.choices?.fa?.answer
                      choiceOneBtn.text = result?.subtitle?.choices?.fa?.distractor

                      choiceOneBtn.setOnClickListener {
                          if (canSetScore) {
                              addToNegative()
                              setScore(UserHandler.getScore() - (2 * (timerSeconds - remainTime)))
                              canSetScore = false
                          }
                      }

                      choiceTwoBtn.setOnClickListener {
                          if (canSetScore) {
                              addToPositive()
                              setScore(UserHandler.getScore() + (2 * (remainTime)))
                              canSetScore = false
                          }
                      }
                  }

                  lastScreenMode = ScreenMode.ChooseWithoutSub
              }
              ScreenMode.ShowTrueAnswer -> {
                  replayBtn.visibility = LinearLayout.VISIBLE
                  llNext.visibility = LinearLayout.VISIBLE
                  tvSubtitle.visibility = TextView.VISIBLE
                  menuBtn.visibility = TextView.VISIBLE
                  llTimer.visibility = LinearLayout.VISIBLE
                  llScore.visibility = LinearLayout.VISIBLE
                  btnContainerLay.visibility = LinearLayout.VISIBLE

                  ic_timer.cancel()

                  llNext.setOnClickListener {
                      llNext.isEnabled = false
                      progressBar.visibility = View.VISIBLE
                      val mdl = SetScoreRequestMdl()
                      mdl.appId = "ir.asemanltd.app.lingotv"
                      mdl.score = UserHandler.getScore().toString()
                      LTH.eLog("_____","app id : "+mdl.appId +" score : " +mdl.score)
                      mdl.metaKey.answer = isRightAnswerChoosed.toString()
                      mdl.metaKey.timeRemaining = remainTime.toString()
                      mdl.metaKeys.falseX = UserHandler.getFalsesCount().toString()
                      mdl.metaKeys.trueX = UserHandler.getTruesCount().toString()
                      mdl.id = result!!.id
                      presenter.setScore(mdl)
                  }

                  choiceTwoBtn.setOnClickListener {}
                  choiceOneBtn.setOnClickListener {}

                  if (isChoiceOneTrue) {
                      choiceOneBtn.text = result?.subtitle?.choices?.fa?.answer
                      choiceTwoBtn.text = result?.subtitle?.choices?.fa?.distractor

                      if (isRightAnswerChoosed) {
                          if (isTimeOut)
                              choiceOneBtn.backgroundTintList = ColorStateList.valueOf(getColor(context!!, R.color.yellow_DD))
                          else
                              choiceOneBtn.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#16a085"))
                      } else
                          choiceTwoBtn.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#c0392b"))

                  } else {
                      choiceTwoBtn.text = result?.subtitle?.choices?.fa?.answer
                      choiceOneBtn.text = result?.subtitle?.choices?.fa?.distractor

                      if (isRightAnswerChoosed) {
                          if (isTimeOut)
                              choiceTwoBtn.backgroundTintList = ColorStateList.valueOf(getColor(context!!, R.color.yellow_DD))
                          else
                              choiceTwoBtn.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#16a085"))
                      } else
                          choiceOneBtn.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#c0392b"))
                  }

                  choiceTwoBtn.setTextColor(Color.WHITE)
                  choiceOneBtn.setTextColor(Color.WHITE)

                  choiceTwoBtn.animate().setDuration(duration.toLong()).translationY(0f).start()
                  choiceOneBtn.animate().setDuration(duration.toLong()).translationY(0f).start()

                  lastScreenMode = ScreenMode.ShowTrueAnswer
              }*/

            ScreenMode.TimeOver -> {
                if (isChoiceLeftTrue) {
                    rightAnswer.performClick()
                   /* rightAnswer.backgroundTintList =
                        ColorStateList.valueOf(ContextCompat.getColor(context, R.color.redL))
                    rightAnswer.setTextColor(Color.WHITE)
                    canSetScore = false
                    addToNegative()*/
                } else {
                    leftAnswer.performClick()
                   /* leftAnswer.backgroundTintList =
                        ColorStateList.valueOf(ContextCompat.getColor(context, R.color.redL))
                    leftAnswer.setTextColor(Color.WHITE)
                    addToNegative()
                    //setScore(UserHandler.getScore() + (1 * (remainTime)))
                    canSetScore = false*/
                }


            }

        }

    }

    private fun addToPositive() {
        afterChoose()
        timer.cancel()

        //isRightAnswerChoosed = true
       // isTimeOut = false
        //val last = UserHandler.getTruesCount()
        //UserHandler.setTruesCount(last + 1)
        //(activity as MainActivity).updateMenu()
    }

    private fun addToNegative() {
        afterChoose()
        timer.cancel()



        //isRightAnswerChoosed = false
        //     isTimeOut = false
        //val last = UserHandler.getFalsesCount()
        //UserHandler.setFalsesCount(last + 1)
        //(activity as MainActivity).updateMenu()
    }
}

