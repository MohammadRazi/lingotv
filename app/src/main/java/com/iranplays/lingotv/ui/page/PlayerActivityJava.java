package com.iranplays.lingotv.ui.page;

import android.content.Intent;
import android.os.Bundle;

import com.iranplays.lingotv.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class PlayerActivityJava extends AppCompatActivity {
    boolean isDark;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        /* get theme */
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            isDark = true;
            setTheme(R.style.DarkTheme);
        } else
            setTheme(R.style.LightTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);


        findViewById(R.id.bPlay).setOnClickListener(v -> {
            if (isDark)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            else
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            startActivity(new Intent(getApplicationContext(), PlayerActivityJava.class));
            finish();
        });
    }
}
