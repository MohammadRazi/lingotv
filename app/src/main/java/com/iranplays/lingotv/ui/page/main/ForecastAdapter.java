package com.iranplays.lingotv.ui.page.main;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.iranplays.lingotv.R;
import com.iranplays.lingotv.utils.GlideApp;
import java.util.ArrayList;
import java.util.List;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private RecyclerView parentRecycler;
    private List<TestPackageModel> data;

    public ForecastAdapter(List<TestPackageModel> data) {

        this.data = data;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentRecycler = recyclerView;
        ArrayList<TestPackageModel> list = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_main_package, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (position < data.size()) {
            holder.progressBar.setVisibility(View.VISIBLE);
            GlideApp.with(holder.itemView.getContext())
                    .load(data.get(position).getUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .transforms(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(holder.itemView.getContext().getResources().getDimensionPixelSize(R.dimen._10sdp))))
                    .into(holder.imageView);

        } else holder.imageView.setImageResource(R.drawable.bg_image);

        GlideApp.with(holder.itemView.getContext())
                 .load(R.drawable.bg_image)
                .transforms(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(holder.itemView.getContext().getResources().getDimensionPixelSize(R.dimen._10sdp))))
                .into(holder.bgOfImage);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView,bgOfImage;
        private CardView cardView;
        private ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.city_image);
            cardView = itemView.findViewById(R.id.cardView);
            bgOfImage = itemView.findViewById(R.id.bgOfImage);
            progressBar = itemView.findViewById(R.id.progressBar);

            cardView.setCardBackgroundColor(Color.parseColor("#16DBDBDB"));

            itemView.findViewById(R.id.container).setOnClickListener(this);
        }

        public void focousItem() {
            cardView.setCardBackgroundColor(Color.parseColor("#EB3B5A"));
        }

        public void unFocousItem() {
            cardView.setCardBackgroundColor(Color.parseColor("#16DBDBDB"));
        }

        @Override
        public void onClick(View v) {
            parentRecycler.smoothScrollToPosition(getAdapterPosition());

        }
    }


}
