package com.iranplays.lingotv.ui.page.main;

public class TestPackageModel {
  private String url;
  private String title_course;
  private String detail_course;
  private String level;

    public String getTitle_course() {
        return title_course;
    }

    public void setTitle_course(String title_course) {
        this.title_course = title_course;
    }

    public String getDetail_course() {
        return detail_course;
    }

    public void setDetail_course(String detail_course) {
        this.detail_course = detail_course;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
