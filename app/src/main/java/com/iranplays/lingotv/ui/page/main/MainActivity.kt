package com.iranplays.lingotv.ui.page.main


import android.content.Intent
import android.view.View
import com.example.basearchitecture.base.BestUtility.AnimUtils
import com.example.basearchitecture.base.EventBusModel
import com.example.basearchitecture.base.baseMvp.BaseActivity
import com.iranplays.lingotv.ui.page.PlayerActivity
import com.iranplays.lingotv.R
import com.iranplays.lingotv.TestDialog
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_player.*


class MainActivity : BaseActivity(), DiscreteScrollView.ScrollStateChangeListener<ForecastAdapter.ViewHolder>, DiscreteScrollView.OnItemChangedListener<ForecastAdapter.ViewHolder> {

    private var list = ArrayList<TestPackageModel>()
    override var contentViewId: Int = R.layout.activity_main

    //region recycler events
    override fun onScroll(position: Float, currentIndex: Int, newIndex: Int, currentHolder: ForecastAdapter.ViewHolder?, newHolder: ForecastAdapter.ViewHolder?) {
        level.visibility = View.INVISIBLE
        titleCourse.visibility = View.INVISIBLE
        coursePercent.visibility = View.INVISIBLE
    }

    override fun onScrollEnd(p0: ForecastAdapter.ViewHolder, p1: Int) {
    }

    override fun onScrollStart(holder: ForecastAdapter.ViewHolder, position: Int) {
        holder?.unFocousItem()
        AnimUtils.showView(this, level, 0, R.anim.fade_out)
        AnimUtils.showView(this, titleCourse, 0, R.anim.fade_out)
        AnimUtils.showView(this, coursePercent, 0, R.anim.fade_out)
    }

    override fun onCurrentItemChanged(holder: ForecastAdapter.ViewHolder?, position: Int) {
        holder?.focousItem()

        level.visibility = View.VISIBLE
        titleCourse.visibility = View.VISIBLE
        coursePercent.visibility = View.VISIBLE

        level.text = list[position].detail_course
        titleCourse.text = list[position].title_course
        coursePercent.text = list[position].level

        AnimUtils.showView(this, level, 0, R.anim.fade_in)
        AnimUtils.showView(this, titleCourse, 0, R.anim.fade_in)
        AnimUtils.showView(this, coursePercent, 0, R.anim.fade_in)
    }
    //endregion

    override fun onEvent(event: EventBusModel) {
    }

    override fun initBeforeView() {


    }

    override fun initNavigation() {

    }

    override fun initToolbar() {

    }

    override fun initViews() {
        //region Delete this Shits

//https://image.freepik.com/free-vector/abstract-wooden-texture_1097-1182.jpg
        val test = TestPackageModel()
        test.url = "https://image.freepik.com/free-vector/abstract-wooden-texture_1097-1182.jpg"
        test.level = "50%"
        test.title_course = "Abstract"
        test.detail_course = "DetailAbstract"

        val test1 = TestPackageModel()
        test1.url = "https://image.freepik.com/free-vector/tropical-foliage-illustration_53876-75800.jpg"
        test1.level = "54%"
        test1.title_course = "Living"
        test1.detail_course = "DetailLiving"

        val test8 = TestPackageModel()
        test8.url = "https://image.freepik.com/free-vector/triangle-background_23-2148008115.jpg?1"
        test8.level = "100%"
        test8.title_course = "English504"
        test8.detail_course = "Vocab"

        val test3 = TestPackageModel()
        test3.url = "https://image.freepik.com/free-vector/two-japanese-koi-fish-swimming_53876-16876.jpg"
        test3.level = "40%"
        test3.title_course = "EnglishGrammer"
        test3.detail_course = "IELTS"

        val test4 = TestPackageModel()
        test4.url = "https://image.freepik.com/free-vector/abstract-background-with-metallic-rose-gold-texture_1048-7968.jpg"
        test4.level = "53%"
        test4.title_course = "Vocab"
        test4.detail_course = "TOFEL"

        val test5 = TestPackageModel()
        test5.url = "https://image.freepik.com/free-vector/hand-drawing-illustration-hipster-style-concept_53876-35548.jpg"
        test5.url = "https://image.freepik.com/free-vector/triangle-background_23-2148008115.jpg?1"
        test5.level = "23%"
        test5.title_course = "English504"
        test5.detail_course = "Vocab"


        val test6 = TestPackageModel()
        test6.url = "https://image.freepik.com/free-vector/hand-drawing-illustration-creative-ideas-concept_53876-37368.jpg"
        test6.url = "https://image.freepik.com/free-vector/triangle-background_23-2148008115.jpg?1"
        test6.level = "85%"
        test6.title_course = "English504"
        test6.detail_course = "Vocab"

        val test7 = TestPackageModel()
        test7.url = "https://image.freepik.com/free-vector/mascot-logo-astronaunt-space-galaxy_50290-68.jpg"
        test7.level = "60%"
        test7.title_course = "Vocab"
        test7.detail_course = "TOFEL"

        val test2 = TestPackageModel()
        test2.url = "https://image.freepik.com/free-vector/fabulous-tropical-pattern-with-green-plants-white-background_24972-138.jpg"
        test2.level = "90%"
        test2.title_course = "English504"
        test2.detail_course = "Vocab"

        list.add(test)
        list.add(test1)
        list.add(test2)
        list.add(test3)
        list.add(test4)
        list.add(test5)
        list.add(test6)
        list.add(test7)
        list.add(test8)
        val forecast = ForecastAdapter(list)
//endregion

        rvCategory.adapter = forecast
        rvCategory.setSlideOnFling(true)
        rvCategory.isHorizontalFadingEdgeEnabled = true
        rvCategory.setItemTransitionTimeMillis(180)
        rvCategory.addOnItemChangedListener(this)
        rvCategory.addScrollStateChangeListener(this)
        rvCategory.setItemTransformer(
                ScaleTransformer.Builder()
                        .setMinScale(0.8f)
                        .build())

        ivProfile.setOnClickListener { TestDialog.newInstance().show(supportFragmentManager,"profile_dialog") }
        btnStart.setOnClickListener {  val intent = Intent(this, com.iranplays.lingotv.ui.page.PlayerActivity::class.java);startActivity(intent)}

    }


}


