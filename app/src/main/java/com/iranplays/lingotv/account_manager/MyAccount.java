package com.iranplays.lingotv.account_manager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.os.Build;
import android.os.Bundle;

import com.example.basearchitecture.base.BestUtility.LogApp;
import com.example.basearchitecture.base.EventBusModel;
import com.iranplays.lingotv.MyApp;
import org.greenrobot.eventbus.EventBus;

public class MyAccount {
    //region public static final variables
    public static final String ID = "ID";
    public static final String USERNAME = "USERNAME";
    public static final String TELEPHONE = "TELEPHONE";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String BIRTHDAY = "BIRTHDAY";
    public static final String SEX = "SEX";
    public static final String PIC = "PIC";
    public static final String CONFIG = "CONFIG";
    public static final String LAST_LOGIN = "LAST_LOGIN";
    public static final String CREATED_AT = "CREATED_AT";
    public static final String UPDATED_AT = "UPDATED_AT";

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String EXPIRE_TOKEN = "EXPIRE_TOKEN";
    public static final String SCOPE_TOKEN = "SCOPE_TOKEN";
    public static final String TYPE_TOKEN = "TYPE_TOKEN";

    public static final String ACCOUNT_TYPE = AccountAuthenticatorConstant.ACCOUNT_TYPE;
    //endregion

    //region private static variables
    private static MyAccount myAccount = null;
    //endregion

    //region private variables
    private String id;
    private String username;
    private String telephone;
    private String name;
    private String email;
    private String birthday;
    private String sex;
    private String pic;
    private String config;
    private String last_login;
    private String created_at;
    private String updated_at;
    private String accessToken;
    private String refreshToken;
    private String expireToken;
    private String scopeToken;
    private String typeToken;
    private Account account = null;
    private AccountManager accountManager = null;

    private MyAccount() {
        accountManager = AccountManager.get(MyApp.getAppInstance());
        LogApp.d("MyAccount", "Create account manager");

        initAccount();

        if (account != null) {
            onAddAccount(account);
        } else {
            //            if (nothing) {
            //                return;
            //            }
            //            LogApp.d(DebugConstants.MY_ACCOUNT, "Request addAcount from accountManager");
            //            accountManager.addAccount(ACCOUNT_TYPE, AccountAuthenticatorConstant.AUTHTOKEN_TYPE_FULL_ACCESS, null, null, iActivity, new AccountManagerCallback<Bundle>() {
            //                @Override
            //                public void run(AccountManagerFuture<Bundle> future) {
            //                    try {
            //                        if (future != null) {
            //                            if (accountManager == null)
            //                                accountManager = AccountManager.get(MyApplication.getInstance());
            //
            //                            Account[] accounts = accountManager.getAccountsByType(AccountAuthenticatorConstant.ACCOUNT_TYPE);
            //
            //                            if (accounts.length > 0) {
            //                                account = accounts[0];
            //                                onAddAccount(account);
            //                                isLogin = true;
            //                            }
            //                        }
            //                    } catch (Exception e) {
            //                        e.printStackTrace();
            //                    }
            //                }
            //            }, null);
        }
    }

    public static MyAccount getInstance() {
        if (myAccount != null && myAccount.account != null) {
            return myAccount;
        }

        MyAccount tmpAccount = new MyAccount();

        if (tmpAccount.account == null) {
            return null;
        }

        myAccount = tmpAccount;

        return myAccount;
    }
    //endregion

    //region getters and setters
    //    public ProfileModel getConfigModel() {
    //        return gson.fromJson(getConfig(), new TypeToken<ProfileConfigModel>() {}.getType());
    //    }

    // TODO: 1/28/2019 pass profile model
   /* public void setProfile(ProfileModel profileModel) {
        LogApp.w("TAG", "MyAccount_setProfile_109-> :" + profileModel.toString());

        setName(profileModel.getName());
        setEmail(profileModel.getEmail());
        setBirthday(profileModel.getBirth());
        setSex(profileModel.getSex());

        if (profileModel.getAvatar() != null && profileModel.getAvatar().getBase() != null) {
            setPic(profileModel.getAvatar().getBase());
        }

        saveUserData();
    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getExpireToken() {
        return expireToken;
    }

    public void setExpireToken(String expireToken) {
        this.expireToken = expireToken;
    }

    public String getScopeToken() {
        return scopeToken;
    }

    public void setScopeToken(String scopeToken) {
        this.scopeToken = scopeToken;
    }

    public String getTypeToken() {
        return typeToken;
    }

    public void setTypeToken(String typeToken) {
        this.typeToken = typeToken;
    }

    public String getUsername() {
        return username;
    }

    private Account getAccount() {
        return account;
    }
    //endregion

    //region functions
    private void setProfileInfo() {

    }

    private void setAuthenticationInfo() {

    }

    private void enableSync() {
        //        ContentResolver.setIsSyncable(My.getAccount(), AccountAuthenticatorConstant.CONTACT_CONTENT_AUTHORITY, 1);
        //        LogApp.d(DebugConstants.MY_ACCOUNT, "Enable sync contacts");
        //        ContentResolver.setSyncAutomatically(myAccount.getAccount(), AccountAuthenticatorConstant.CONTACT_CONTENT_AUTHORITY, true);
        //        LogApp.d(DebugConstants.MY_ACCOUNT, "Enable automatically sync contacts");
    }

    private void disableSync() {
        ContentResolver.setIsSyncable(myAccount.getAccount(), AccountAuthenticatorConstant.CONTACT_CONTENT_AUTHORITY, 0);
        ContentResolver.setSyncAutomatically(myAccount.getAccount(), AccountAuthenticatorConstant.CONTACT_CONTENT_AUTHORITY, false);
    }

    private void initAccount() {
        LogApp.d("MyAccount", "initAccount number of accounts:" + accountManager.getAccountsByType(ACCOUNT_TYPE).length);
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        account = (accounts.length > 0) ? accounts[0] : null;
    }

    private void onAddAccount(Account account) {
        LogApp.d("MyAccount", "account is not null in MyApplication:" + account + " nothing:");

        username = account.name;
        telephone = accountManager.getUserData(account, TELEPHONE);
        name = accountManager.getUserData(account, NAME);
        email = accountManager.getUserData(account, EMAIL);
        birthday = accountManager.getUserData(account, BIRTHDAY);
        sex = accountManager.getUserData(account, SEX);
        pic = accountManager.getUserData(account, PIC);
        config = accountManager.getUserData(account, CONFIG);
        last_login = accountManager.getUserData(account, LAST_LOGIN);
        created_at = accountManager.getUserData(account, CREATED_AT);
        updated_at = accountManager.getUserData(account, UPDATED_AT);

        accessToken = accountManager.getUserData(account, ACCESS_TOKEN);
        refreshToken = accountManager.getUserData(account, REFRESH_TOKEN);
        expireToken = accountManager.getUserData(account, EXPIRE_TOKEN);
        scopeToken = accountManager.getUserData(account, SCOPE_TOKEN);
        typeToken = accountManager.getUserData(account, TYPE_TOKEN);

        LogApp.d("MyAccount", "account is not null in MyApplication 2 :" + account);

        if (myAccount != null) {
            enableSync();
        }
    }

    public void saveUserData() {
        LogApp.d("MyAccount", "saveUserData");

        accountManager.setUserData(account, ID, this.getId());
        accountManager.setUserData(account, USERNAME, this.getUsername());
        accountManager.setUserData(account, TELEPHONE, this.getTelephone());
        accountManager.setUserData(account, NAME, this.getName());
        accountManager.setUserData(account, EMAIL, this.getEmail());
        accountManager.setUserData(account, BIRTHDAY, this.getBirthday());
        accountManager.setUserData(account, SEX, this.getSex());
        accountManager.setUserData(account, PIC, this.getPic());
        accountManager.setUserData(account, CONFIG, this.getConfig());
        accountManager.setUserData(account, LAST_LOGIN, this.getLast_login());
        accountManager.setUserData(account, CREATED_AT, this.getCreated_at());
        accountManager.setUserData(account, UPDATED_AT, this.getUpdated_at());

        accountManager.setUserData(account, ACCESS_TOKEN, this.getAccessToken());
        accountManager.setUserData(account, REFRESH_TOKEN, this.getRefreshToken());
        accountManager.setUserData(account, EXPIRE_TOKEN, this.getExpireToken());
        accountManager.setUserData(account, SCOPE_TOKEN, this.getScopeToken());
        accountManager.setUserData(account, TYPE_TOKEN, this.getTypeToken());

        //        return;
        //        new AsyncTask<MyAccount, Void, Void>() {
        //            @Override
        //            protected Void doInBackground(MyAccount... params) {
        //                LogApp.w(DebugConstants.WORKFLOW_TAG, "doInBackground setUserData");
        //                accountManager.setUserData(account, FIRST_NAME, params[0].getFirstName());
        //                accountManager.setUserData(account, LAST_NAME, params[0].getLastName());
        //                accountManager.setUserData(account, EMAIL_ADDRESS, params[0].getEmailAddress());
        //                accountManager.setUserData(account, USER_IMAGE_PATH, params[0].getUserImagePath());
        //                accountManager.setUserData(account, ORIGINAL_IMAGE_ADDRESS, params[0].getOriginalImagePath());
        //                accountManager.setUserData(account, THUMBNAIL_IMAGE_ADDRESS, params[0].getThumbnailImagePath());
        //                accountManager.setUserData(account, COUNTRY_CODE, params[0].getCountryCode());
        //                accountManager.setUserData(account, STATUS, params[0].getStatus());
        //                accountManager.setUserData(account, SET_PROFILE, params[0].getSetProfile());
        //
        //                LogApp.e("MyAccount1", "saveUserData - FirstName: " + params[0].getFirstName() + "    LastName: " + params[0].getLastName() + "    imgURL: " + params[0].getOriginalImagePath());
        //                LogApp.w("MyAccount22", "saveUserData - FirstName: " + params[0].getFirstName() + "    LastName: " + params[0].getLastName() + "    getCountryCode: " + params[0].getCountryCode());
        //
        //                return null;
        //            }
        //
        //            @Override
        //            protected void onPostExecute(Void aVoid) {
        //                LogApp.w(DebugConstants.WORKFLOW_TAG, "Save user data in accountmanager");
        //            }
        //        }.execute(this);
    }

    private void deInit() {
        accountManager = null;
        account = null;
        myAccount = null;
    }

    private void removeAccount() {
        disableSync();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (account != null) {
                accountManager.removeAccountExplicitly(account);
            }
        } else {
            if (account != null) {
                accountManager.removeAccount(account, null, null);
            }
        }

        deInit();
    }
    //endregion

    public static void setAccount(String _username, _AccountAuthenticationModel _authenticationModel, _AccountProfileModel _profileModel) {
        Account account = new Account(_username, MyAccount.ACCOUNT_TYPE);

        Bundle bundle = new Bundle();
        bundle.putString(MyAccount.USERNAME, _username);

        //region Authentication
        if (_authenticationModel != null) {
            bundle.putString(MyAccount.ACCESS_TOKEN, _authenticationModel.getAccess_token());
            bundle.putString(MyAccount.REFRESH_TOKEN, _authenticationModel.getRefresh_token());
            bundle.putString(MyAccount.EXPIRE_TOKEN, String.valueOf(_authenticationModel.getExpires_in()));
            bundle.putString(MyAccount.SCOPE_TOKEN, _authenticationModel.getScope());
            bundle.putString(MyAccount.TYPE_TOKEN, _authenticationModel.getToken_type());
        }
        //endregion

        //region Profile
        if (_profileModel != null) {
            bundle.putString(MyAccount.ID, _profileModel.getId());
            bundle.putString(MyAccount.TELEPHONE, _profileModel.getTelephone());
            bundle.putString(MyAccount.EMAIL, _profileModel.getEmail());
            bundle.putString(MyAccount.NAME, _profileModel.getName());
            bundle.putString(MyAccount.BIRTHDAY, _profileModel.getBirthday());
            bundle.putString(MyAccount.SEX, _profileModel.getSex());
            bundle.putString(MyAccount.PIC, _profileModel.getPic());
            bundle.putString(MyAccount.CONFIG, _profileModel.getConfig());
            bundle.putString(MyAccount.LAST_LOGIN, _profileModel.getLast_login());
            bundle.putLong(MyAccount.CREATED_AT, _profileModel.getCreated_at());
            bundle.putLong(MyAccount.UPDATED_AT, _profileModel.getUpdated_at());
        }
        //endregion

        AccountManager accountManager = AccountManager.get(MyApp.getAppInstance());
        accountManager.addAccountExplicitly(account, _authenticationModel != null ? _authenticationModel.getAccess_token() : null, bundle);
    }

    public static boolean isLogin() {
        return MyAccount.getInstance() != null && MyAccount.getInstance().getAccessToken() != null;
    }

    public static void Clear() {
        if (MyAccount.getInstance() != null) {
            MyAccount.getInstance().deInit();
        }

        EventBus.getDefault().postSticky(new EventBusModel(EventBusModel.EventCode.LoggedOut, "done!!"));
    }

    public static void LogOut() {
        if (MyAccount.getInstance() != null) {
            MyAccount.getInstance().removeAccount();
        }

        EventBus.getDefault().postSticky(new EventBusModel(EventBusModel.EventCode.LoggedOut, "done!!"));
    }
}
