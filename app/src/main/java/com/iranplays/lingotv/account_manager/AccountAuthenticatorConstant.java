package com.iranplays.lingotv.account_manager;


import com.iranplays.lingotv.MyApp;
import com.iranplays.lingotv.R;


class AccountAuthenticatorConstant {
    final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    final static String CONTACT_CONTENT_AUTHORITY = "com.android.contacts";
    final static String ACCOUNT_TYPE =  MyApp.getAppInstance().getString(R.string.package_namee);
    final static String ACCOUNT_MANAGER_EVENT_BROADCAST = MyApp.getAppInstance().getString(R.string.package_namee) + ".AccountBroadcast";
    final static String ACCOUNT_MANAGER_EVENT_BROADCAST_REMOVED_ACCOUNT = "RemoveAccount";
    final static String ACCOUNT_MANAGER_EVENT_BROADCAST_ADDED_ACCOUNT = "AddAccount";
    final static String PARAM_USER_PASS = "USER_PASS";
    static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    static final String AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only access to account";
    static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to account";
}
