package com.iranplays.lingotv.account_manager;

import android.accounts.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.example.basearchitecture.base.BestUtility.LogApp;
import com.iranplays.lingotv.ui.page.main.MainActivity;

import static com.iranplays.lingotv.account_manager.AccountAuthenticatorConstant.*;


class AccountAuthenticator extends AbstractAccountAuthenticator {
    private static final String _Tag = "AccountAuthenticator";
    private final Context mContext;

    AccountAuthenticator(Context mContext) {
        super(mContext);
        this.mContext = mContext;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        return null;
    }

    @Override
    public Bundle getAccountRemovalAllowed(AccountAuthenticatorResponse response, Account account) throws NetworkErrorException {
        MyAccount.Clear();

        LogApp.d(_Tag, "AccountRemoveAllowed");
        return super.getAccountRemovalAllowed(response, account);
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        LogApp.d(_Tag, "> addAccount");

        Account[] accounts = AccountManager.get(mContext).getAccountsByType(AccountAuthenticatorConstant.ACCOUNT_TYPE);

        if (accounts.length > 0) {
            LogApp.d(_Tag, "> There is one account!");
            return null;
        }

        final Intent intent = new Intent(mContext, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {

        LogApp.d("AccountAuthenticator", "> getAuthToken");
        return null;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        if (AUTHTOKEN_TYPE_FULL_ACCESS.equals(authTokenType))
            return AUTHTOKEN_TYPE_FULL_ACCESS_LABEL;
        else if (AUTHTOKEN_TYPE_READ_ONLY.equals(authTokenType))
            return AUTHTOKEN_TYPE_READ_ONLY_LABEL;
        else
            return authTokenType + " (Label)";
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }
}








