package com.iranplays.lingotv.account_manager;


import com.example.basearchitecture.base.BaseModel;

public class _AccountProfileModel extends BaseModel {
    private String id;
    private String username;
    private String telephone;
    private String name;
    private String email;
    private String birthday;
    private String sex;
    private String pic;
    private String config;
    private String last_login;
    private long created_at = 0;
    private long updated_at = 0;

    public _AccountProfileModel() {
    }

    public _AccountProfileModel(String id, String username, String telephone, String name, String email, String birthday, String sex, String pic, String config, String last_login, int created_at, int updated_at) {
        this.id = id;
        this.username = username;
        this.telephone = telephone;
        this.name = name;
        this.email = email;
        this.birthday = birthday;
        this.sex = sex;
        this.pic = pic;
        this.config = config;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.last_login = last_login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }
}
