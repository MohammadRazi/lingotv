package com.iranplays.lingotv

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.basearchitecture.base.baseMvp.BaseFragmentDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_login.*

class TestDialog : BaseFragmentDialog() {

    companion object {
        fun newInstance(): TestDialog {
            val args = Bundle()
            val fragment = TestDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun setUp(view: View) {
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_login, container, false)
    }

}