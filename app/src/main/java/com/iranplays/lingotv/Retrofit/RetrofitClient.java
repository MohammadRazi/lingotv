package com.iranplays.lingotv.Retrofit;

import com.example.basearchitecture.BuildConfig;
import com.example.basearchitecture.base.BestUtility.LogApp;
import com.iranplays.lingotv.MyApp;
import com.iranplays.lingotv.Retrofit.Constants.ClientConfigs;
import com.iranplays.lingotv.Retrofit.TypeAdapters.UTCDateTypeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.iranplays.lingotv.account_manager.MyAccount;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class RetrofitClient {
    private static final int TIME_OUT = 120;
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

            //region OkHttpSetting
            okHttpClient.connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build();
            //endregion@

            //region okHttpClient.addInterceptor | Add default parameter to Header
            okHttpClient.addInterceptor(chain -> {
                Request request = chain.request();
                Request.Builder requestBuilder = request.newBuilder();
                requestBuilder.addHeader("Accept", "application/json");
                requestBuilder.addHeader("ApplicationID",  BuildConfig.APPLICATION_ID);

                if (MyAccount.isLogin()) {
                    requestBuilder.addHeader("Authorization", "Bearer " + MyAccount.getInstance().getAccessToken());
                } else {
//                    requestBuilder.addHeader("client_id", ClientConfigs.CLIENT_ID);
//                    requestBuilder.addHeader("client_key", ClientConfigs.CLIENT_KEY);
                }

                return chain.proceed(requestBuilder.build());
            });
            //endregion

            //region Cache
            int cacheSize = 10 * 1024 * 1024; // 10 MB
            Cache cache = new Cache(MyApp.getAppInstance().getCacheDir(), cacheSize);

            okHttpClient.cache(cache).build();
            //endregion

            //region Authorization Error code= 401
            okHttpClient.authenticator((route, response) -> {
                if (MyAccount.isLogin()) {
//                    NotificationCenter.getInstance().broadcastNotification(
//                            new NotificationApplication()
//                                    .AddEvent(NotificationApplication.EVENT_LOGOUT));

                    return null;

                    //make the refresh token request model
//                        RefreshTokenRequestModel refreshTokenRequestModel = new RefreshTokenRequestModel();
//                        refreshTokenRequestModel.setRefresh_token(SPO.getInstance().getAuthenticationInfo().getRefresh_token());

//                        retrofit2.Response<TokenModel> tokenModelResponse = RetrofitClient.getService().RefreshToken("").execute();

//                        if (tokenModelResponse.isSuccessful()) {
//                            SPO.getInstance().setAuthenticationInfo(tokenModelResponse.body());
//                            return response.request().newBuilder()
//                                    .removeHeader("Authorization")
//                                    .addHeader("Authorization", "bearer " + SPO.getInstance().getAuthenticationInfo().getToken())
//                                    .build();
//                        } else {
//                            return null;
//                        }
                } else {
                    return null;
                }
            });
            //endregion

            //region UTCDateTypeAdapter
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new UTCDateTypeAdapter())
                    .create();
            //endregion

            //region Logger Config
//            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> LogApp.w("MyRetrofit", message));
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.addInterceptor(logging).build();
            //endregion

            //region Retrofit.Builder
            retrofit = new Retrofit.Builder()
                    .baseUrl(ClientConfigs.REST_API_BASE_URL)
                    .client(okHttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            //endregion
        }
        return retrofit;
    }

    public static RetrofitService getService() {
        return getClient().create(RetrofitService.class);
    }

    private static String bodyToString(final Request request) {
        try {
            Request copy = request.newBuilder().build();

            if (request.method().equals("POST")) {
                Buffer buffer = new Buffer();
                copy.body().writeTo(buffer);

                return URLDecoder.decode(buffer.readUtf8(), "UTF-8");
            } else {
//                return new Gson().toJson(splitQuery(copy.url().url()));
                return copy.url().url().getQuery();
            }
        } catch (IOException e) {
            return "";
        }
    }
}
