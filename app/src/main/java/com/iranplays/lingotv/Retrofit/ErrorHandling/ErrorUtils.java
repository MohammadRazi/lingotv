package com.iranplays.lingotv.Retrofit.ErrorHandling;

import com.iranplays.lingotv.Retrofit.RetrofitClient;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import okhttp3.ResponseBody;
import retrofit2.Converter;

import java.io.IOException;
import java.lang.annotation.Annotation;

public class ErrorUtils {
    private static ErrorModel parseError(ResponseBody response) throws IOException {
        Converter<ResponseBody, ErrorModel> converter = RetrofitClient.getClient().responseBodyConverter(ErrorModel.class, new Annotation[0]);
        return converter.convert(response);
    }

//    public static String getErrorMessage(Response<?> response) {
//        if (response.code() == 503) {
//            return "سرور در حال بروزرسانی!" + "\n" + "به زودی بر میگردیم";
//        } else {
//            Converter<ResponseBody, ErrorModel> converter = RetrofitClient.getClient().responseBodyConverter(ErrorModel.class, new Annotation[0]);
//
//            ErrorModel o_error = null;
//            String msg = "";
//
//            try {
//                o_error = converter.convert(response.errorBody());
//
//                if (response.code() == 500) {
//                    msg = "در حال حاضر سرور قادر به پاسخگویی نمی باشد!" + "\n" + "لطفا مجددا تلاش کنید.";
//                } else {
//                    msg = o_error.getMsg() != null ? o_error.getMsg() : "در حال حاضر سرور قادر به پاسخگویی نمی باشد!" + "\n" + "لطفا مجددا تلاش کنید.";
//                }
//            } catch (IOException e) {
//                msg = "در حال حاضر سرور قادر به پاسخگویی نمی باشد!" + "\n" + "لطفا مجددا تلاش کنید.";
//            }
//
////            sendErrorDetailsToServer(response, o_error, msg);
//
//            LogApp.e("Retrofit", "ErrorUtils=> Code= " + response.code() + " , Message= " + msg);
//
//            return msg;
//        }
//    }

    public static String getErrorMessage(Throwable e) {
        String msg = "ارتباط با سرور برقرار نشد!";

        if (e instanceof HttpException) {
            msg = ErrorUtils.getError((HttpException) e).getMessage();
        }

        return msg;
    }

    public static ErrorModel getErrorModel(Throwable e) {
        ErrorModel errorModel = new ErrorModel();
        errorModel.setMessage("ارتباط با سرور برقرار نشد!");

        if (e instanceof HttpException) {
            errorModel.setMessage(ErrorUtils.getError((HttpException) e).getMessage());
        }

        return errorModel;
    }

    private static ErrorModel getError(HttpException e) {
        ErrorModel errorModel = new ErrorModel();
        errorModel.setMessage("ارتباط با سرور برقرار نشد!");

        if (e.code() == 503) {
            errorModel.setMessage("سرور در حال بروزرسانی!" + "\n" + "به زودی بر میگردیم");
        } else {
            try {
                ErrorModel errorResult = parseError(e.response().errorBody());
                errorModel.setMessage(errorResult.getMessage() != null ? errorResult.getMessage() : "در حال حاضر سرور قادر به پاسخگویی نمی باشد!" + "\n" + "لطفا مجددا تلاش کنید.");
            } catch (IOException e1) {
                errorModel.setMessage("در حال حاضر سرور قادر به پاسخگویی نمی باشد!" + "\n" + "لطفا مجددا تلاش کنید.");
            }
        }

        return errorModel;
    }
}
