package com.iranplays.lingotv.Retrofit;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitService {
    @FormUrlEncoded
    @POST("user/login-by-telephone")
    Observable<ResponseBody> login(
            @Field("telephone") String telephone,
            @Field("fcm") String fcm);

 /*   @FormUrlEncoded
    @POST("user/login")
    Observable<LoginModel> loginVerify(
            @Field("telephone") String mobile,
            @Field("code") String code);

    @GET("user/get-profile")
    Observable<ProfileResponseModel> getProfile();

    @Multipart
    @POST("user/edit-profile")
    Call<ProfileResponseModel> setProfile(@Part MultipartBody.Part file,
                                          @Part("name") RequestBody name,
                                          @Part("email") RequestBody email,
                                          @Part("sex") RequestBody sex,
                                          @Part("birth") RequestBody birthday);

    @POST("user/edit-profile")
    Observable<ProfileResponseModel> setProfile(@Body ProfileModel data);

    @FormUrlEncoded
    @POST("user/edit-profile")
    Observable<ProfileResponseModel> setConfig(@Field("config") ProfileConfigModel data);


    @GET("layout/" + Constants.Layout)
    Observable<DashboardResponseModel> getLayouts(@Query("tab") String tab);

    @FormUrlEncoded
    @POST("message")
    Observable<MessageSendResponseModel> sendMessage(@Field("message") String message);

    @GET("message")
    Observable<MessageResponseModel> getMessages(
            @Query("offset") String offset,
            @Query("limit") String limit);

    @GET("category/{id}")
    Observable<ProductCategoryResponseModel> getCategoryItems(
            @Path("id") String id,
            @Query("offset") String offset,
            @Query("limit") String limit,
            @Query("min_type") String min_type);

    @GET("product/get-product-related")
    Observable<ProductCategoryResponseModel> getRelatedItems(
            @Query("id") String id,
            @Query("offset") String offset,
            @Query("limit") String limit,
            @Query("min_type") String min_type);

    @GET("publisher/list-products")
    Observable<ProductCategoryResponseModel> getPublisherItems(
            @Query("id") String id,
            @Query("offset") String offset,
            @Query("limit") String limit,
            @Query("min_type") String min_type);

    @GET("wish-list")
    Observable<ProductCategoryResponseModel> getWishListItems(
            @Query("offset") String offset,
            @Query("limit") String limit,
            @Query("min_type") String min_type);

    @GET("search/search-data")
    Observable<ProductCategoryResponseModel> search(
            @Query("name") String search,
            @Query("offset") String offset,
            @Query("limit") String limit,
            @Query("min_type") String min_type);

    @GET("search/search-data")
    Observable<ProductCategoryResponseModel> search(
            @Query("name") String search,
            @Query("offset") String offset,
            @Query("limit") String limit);

    @GET("search/search-data")
    Call<ProductCategoryResponseModel> search(@Query("name") String search);

    @GET("product/{id}")
    Observable<ProductDetailsResponseModel> getMainDetails(
            @Path("id") String id,
            @Query("fields") String fields);

    @FormUrlEncoded
    @POST("product/installed")
    Observable<Response<Void>> installedAppCounterAPI(
            @Field("id") String id,
            @Field("status") String status,
            @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("wish-list")
    Observable<WishListModel> updateBookmarkAPI(@Field("id") String id);

    @FormUrlEncoded
    @POST("wish-list/is-exist")
    Observable<Boolean> isBookmark(@Field("id") String id);

    @GET("comment/{id}")
    Observable<UserCommentModel> getUserCommentAPI(@Path("id") String id);

    @FormUrlEncoded
    @POST("comment/toggle-like-comment")
    Observable<LikeDislikeCommentModel> sendLikeDislikeCommentAPI(
            @Field("comment_id") String comment_id,
            @Field("like") String message);

    @FormUrlEncoded
    @POST("comment")
    Observable<Response<Void>> sendUserCommentAPI(
            @Field("product_id") String product_id,
            @Field("message") String message,
            @Field("rate") String rate);
*/
}
