package com.iranplays.lingotv.Retrofit.FileServer;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ProgressRequestBody extends RequestBody {
    private static final int DEFAULT_BUFFER_SIZE = 2048;
    private File mFile;
    //    private String mPath;
    private UploadCallbacks mListener;

    public ProgressRequestBody(final File file, final UploadCallbacks listener) {
        mFile = file;
        mListener = listener;
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse("file/*");
    }

    @Override
    public long contentLength() throws IOException {
        return mFile.length();
    }

    @Override
    public void writeTo(BufferedSink sink) {
        long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long uploaded = 0;
        FileInputStream in = null;

        try {
            in = new FileInputStream(mFile);

            int read;
            while ((read = in.read(buffer)) != -1) {
                mListener.onProgressUpdate((int) (100 * uploaded / fileLength));

                uploaded += read;
                sink.write(buffer, 0, read);
            }

            mListener.onProgressUpdate(100);
            mListener.onFinish();
        } catch (Exception e) {
            mListener.onError();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                mListener.onError();
            }
        }
    }

    public interface UploadCallbacks {
        void onProgressUpdate(int percentage);

        void onError();

        void onFinish();
    }
}