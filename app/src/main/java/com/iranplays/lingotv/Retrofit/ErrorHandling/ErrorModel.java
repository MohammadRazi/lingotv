package com.iranplays.lingotv.Retrofit.ErrorHandling;


import com.example.basearchitecture.base.BaseModel;

public class ErrorModel extends BaseModel {
    private String message;
//    private String result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public String getResult() {
//        return result;
//    }
//
//    public void setResult(String result) {
//        this.result = result;
//    }
}
