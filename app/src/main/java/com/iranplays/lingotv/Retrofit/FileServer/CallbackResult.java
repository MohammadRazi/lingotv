package com.iranplays.lingotv.Retrofit.FileServer;

import org.json.JSONObject;

public interface CallbackResult {
    void onNetworkResponse(JSONObject jsonFile);

    void onProgressChanged(float progress);
}
