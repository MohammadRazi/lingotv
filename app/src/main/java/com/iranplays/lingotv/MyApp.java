package com.iranplays.lingotv;

import android.app.Application;

import com.example.basearchitecture.base.BestUtility.UsefulUtility;

public class MyApp extends Application {



    private static volatile MyApp instance;
    private static final Object mutex = new Object();


    public static MyApp getAppInstance() {
        MyApp result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null) {
                    instance = result = new MyApp();
                }
            }
        }
        return result;
    }


    private static MyApp singleton;

//    public static MyApp getAppInstance() {
//        if (singleton == null) {
//            synchronized (MyApp.class) {
//                if (singleton == null) {
//                    singleton = new MyApp();
//                }
//            }
//        }
//        return singleton;
//    }

//    public static MyApp getAppInstance() {
//        if (instance == null) {
////            synchronized (MyApp.class) {
////                if (instance == null) {
//                    instance = new MyApp();
////                }
////            }
//        }
//        return instance;
//    }


//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        Glide.get(this).clearMemory();
//    }

    @Override
    public void onCreate() {
        super.onCreate();
//        singleton = this;
        UsefulUtility.init(this);


    }


}
