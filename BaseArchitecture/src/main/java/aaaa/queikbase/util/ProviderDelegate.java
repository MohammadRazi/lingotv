package aaaa.queikbase.util;

import aaaa.queikbase.provider.BaseItemProvider;
import android.util.SparseArray;

/**
 * https://github.com/chaychan
 * @author ChayChan
 * @date 2018/3/21  11:04
 */

public class ProviderDelegate {

    private SparseArray<aaaa.queikbase.provider.BaseItemProvider> mItemProviders = new SparseArray<>();

    public void registerProvider(aaaa.queikbase.provider.BaseItemProvider provider){
        if (provider == null){
            throw new ItemProviderException("ItemProvider can not be null");
        }

        int viewType = provider.viewType();

        if (mItemProviders.get(viewType) == null){
            mItemProviders.put(viewType,provider);
        }
    }

    public SparseArray<BaseItemProvider> getItemProviders(){
        return mItemProviders;
    }

}
