package aaaa.queikbase.entity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public interface MultiItemEntity {
    int getItemType();
}
