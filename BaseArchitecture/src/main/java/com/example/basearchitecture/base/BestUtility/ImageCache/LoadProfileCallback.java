package com.example.basearchitecture.base.BestUtility.ImageCache;

/**
 * Created by NoBodi on 2016/6/sticker AD.
 */
public interface LoadProfileCallback {
    void onLoadProfilePicture(boolean loaded);
}