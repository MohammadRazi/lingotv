package com.example.basearchitecture.base.BestUtility.ImageCache;

import android.graphics.Bitmap;
import com.example.basearchitecture.base.BestUtility.LogApp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageCache {

    private static ImageCache ourInstance = null;

    public ImageCache() {
    }

    public String getOriginalUrl(String url) {
        if (url != null) {
            url = url.replace("/images/", "/images/original/");
            LogApp.w("ConvertURL", "getOriginalUrl= " + url);
        }
        return url;
    }

    public String getThumbnailUrl(String url) {
        if (url != null) {
            url = url.replace("/images/", "/images/thumbnail/");
            LogApp.w("ConvertURL", "getThumbnailUrl= " + url);
        }
        return url;
    }

//    @SuppressLint("CheckResult")
//    public void loadUrlByGlide(String url, final ImageView imageView, ImageType imageType) {
//        if (url != null) {
//            if (imageType == ImageType.Round) {
//                //region Glide
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
//                requestOptions.placeholder(R.drawable.ic_user);
//                requestOptions.error(R.drawable.ic_user);
//                requestOptions.circleCrop();
//
//                Glide.with(MyApplication.getInstance())
//                        .applyDefaultRequestOptions(requestOptions)
//                        .load(url)
//                        .into(imageView);
//                //endregion
//            } else {
//                //region Glide
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
//                requestOptions.placeholder(R.drawable.ic_user);
//                requestOptions.error(R.drawable.ic_user);
//
//                Glide.with(MyApplication.getInstance())
//                        .applyDefaultRequestOptions(requestOptions)
//                        .load(url)
//                        .into(imageView);
//                //endregion
//            }
//        }
//    }

//    @SuppressLint("CheckResult")
//    private void GlideTransform(Activity activity, ImageView imageView) {
//        // loading header background image
//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
//        requestOptions.error(R.drawable.bg_drawer_top);
//        requestOptions.placeholder(R.drawable.bg_drawer_top);
//        requestOptions.fallback(R.drawable.bg_drawer_top);
//
//        Glide.with(activity)
//                .applyDefaultRequestOptions(requestOptions)
//                .load(SPO.getInstance().getProfilePicOriginal())
//                .apply(new RequestOptions().transforms(new BlurTransformation(200)))
//                .transition(withCrossFade())
//                .into(imageView);
//
//        // loading header user image
//
//        requestOptions = new RequestOptions();
//        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
//        requestOptions.error(R.drawable.ic_user);
//        requestOptions.placeholder(R.drawable.ic_user);
//        requestOptions.fallback(R.drawable.ic_user);
//        requestOptions.circleCrop();
//
//        Glide.with(activity)
//                .applyDefaultRequestOptions(requestOptions)
//                .load(SPO.getInstance().getProfilePicOriginal())
//                .transition(withCrossFade())
//                .thumbnail(0.5f)
//                .into(imageView);
//    }
//
//    public void loadUrlByGlide(String url, final ImageView imageView) {
//        if (url != null) {
//            //region Glide
//            Glide.with(MyApplication.getInstance())
//                    .load(url)
////                    .diskCacheStrategy(DiskCacheStrategy.ALL)
////                    .placeholder(R.drawable.ic_user)
////                    .error(R.drawable.ic_user)
//                    .into(imageView);
//            //endregion
//        }
//    }

    public void DownloadImage(final String url, final String localPath) {
        final String TAG = "DownloadImage";
        final Bitmap[] bitmap = new Bitmap[1];

//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... params) {
//                Looper.prepare();
//                try {
//                    bitmap[0] = Glide.with(MyApplication.getInstance())
//                            .load(url)
//                            .asBitmap()
//                            .into(600, 600)
//                            .get();
//                } catch (final ExecutionException | InterruptedException e) {
//                    LogApp.e(TAG, e.getMessage());
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void dummy) {
//                if (bitmap[0] != null) {
//                    SaveBitmapToLocation(localPath, bitmap[0]);
//
//                    LogApp.d(TAG, "Image loaded");
//                }
//            }
//        }.execute();
    }

    public void Download(final String url, final String localPath) {
        final String TAG = "DownloadImage";

        LogApp.d(TAG, "url= " + url + " , localPath= " + localPath);

//        Glide.with(MyApplication.getInstance())
//                .load(url)
//                .downloadOnly(new SimpleTarget<File>() {
//                    @Override
//                    public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
//                        LogApp.d(TAG, "Image loaded size= " + resource.length() + " , path= " + resource.getPath());
//
//                        try {
//                            FileUtility.CopyByChannel(resource.getAbsolutePath(), localPath);
//                            LogApp.w(TAG, "Image loaded size= " + resource.length() + " , path= " + resource.getPath());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            LogApp.e(TAG, "Image loaded size= " + e.getMessage());
//                        }
//                    }
//                });
    }

//    public void DownloadAPK(final String url, final String localPath) {
//        final String TAG = "DownloadImage";
//
//        Glide.with(MyApplication.getInstance())
//                .load(url)
//                .downloadOnly(new SimpleTarget<File>() {
//                    @Override
//                    public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
//                        LogApp.d(TAG, "Image loaded size= " + resource.length() + " , path= " + resource.getPath());
//
//                        try {
//                            FileUtility.CopyByChannel(resource.getAbsolutePath(), localPath);
//                            LogApp.w(TAG, "Image loaded size= " + resource.length() + " , path= " + resource.getPath());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            LogApp.e(TAG, "Image loaded size= " + e.getMessage());
//                        }
//                    }
//                });
//    }

    private void SaveBitmapToLocation(String path, Bitmap bitmap) {
        File file = new File(path);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //region Comment Codes
    //Profile
//    public void loadProfile(final String Username, final ImageView imageView, final JsonContact contact, final ImageSource imageSource, ImageType imageType) {
//        loadProfileByGlide(Username, imageView, contact, imageSource, imageType, true, false, false, null);
//    }
//
//    public void loadProfileByGlide(final String Username, final ImageView imageView, final JsonContact contact, final ImageSource imageSource, ImageType imageType, final boolean getProfile, final boolean NoPhotoInverse, boolean isOriginalProfile, final LoadProfileCallback callback) {
//        //region Set Variable values
//        String imgUrl = "";
//        String displayName = "";
//
//        JsonContact jsonContact = contact;
//        if (jsonContact == null) {
//            jsonContact = dbContact.getInstance().getJsonContact(Username);
//        }
//
//        if (jsonContact != null) {
//            displayName = jsonContact.getContact_Name();
//
//            if (imageSource == ImageSource.Thumbnail) {
//                imgUrl = jsonContact.getContact_ExtraClass().getProfile().getImageThumb();
//            } else {
//                imgUrl = jsonContact.getContact_ExtraClass().getProfile().getImageFull();
//            }
//        }
//
//        if (displayName == null || displayName.trim().equals("")) {
//            displayName = Username;
//        }
//
//        if (imgUrl != null)
//            if (imgUrl.trim().equals(""))
//                imgUrl = null;
//
//        final String finalImgUrl = imgUrl;
//        //endregion
//
//        LogApp.v("Picasso", "displayName= " + displayName + " , URL= " + imgUrl);
//        LogApp.d("Picasso", "--------------------------------------------------------------------");
//
//        final Drawable drawable = ImageUtility.setupUsersImageIfNotPic(displayName, imageType, NoPhotoInverse);
//
//        if (finalImgUrl != null && !finalImgUrl.equals("")) {
//            //region if (isMeuContact)
//            if (imageType == ImageType.Round) {
//                //region ImageType.Round
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
////                                callback.onLoadProfilePicture(true);
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
////                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//                //endregion
//            } else {
//                //region ImageType.Rect
//                LogApp.w("Picasso", "1 --- jsonContact= " + jsonContact.getContact_ExtraClass().getProfile().getImageThumb());
//
//                Glide.with(MyApplication.getInstance())
//                        .load(jsonContact.getContact_ExtraClass().getProfile().getImageThumb())
//                        .bitmapTransform(new BlurTransformation(MyApplication.getInstance(), 10))
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//
//                                LogApp.e("Picasso", "2 --- jsonContact= " + finalImgUrl);
//
////                                //region Load original image
//                                Glide.with(MyApplication.getInstance())
//                                        .load(finalImgUrl)
//                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(resource.getCurrent())
//                                        .error(drawable)
//                                        .into(new GlideDrawableImageViewTarget(imageView) {
//                                            @Override
//                                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                                super.onResourceReady(resource, animation);
//                                                LogApp.d("Picasso", "3 --- onResourceReady= ");
//
//                                                callback.onLoadProfilePicture(true);
//                                            }
//
//                                            @Override
//                                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                                super.onLoadFailed(e, errorDrawable);
//                                                LogApp.d("Picasso", "4 --- onResourceReady= ");
//
//                                                callback.onLoadProfilePicture(false);
//                                            }
//                                        });
////                                //endregion
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//                //endregion
//            }
//            //endregion
//        } else {
//            //region if (! isMeuContact)
//            if (imageType == ImageType.Round) {
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                            }
//                        });
//            } else {
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//                                callback.onLoadProfilePicture(false);
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//            }
//
//            if (getProfile) {
//                loadProfileFromServer(Username);
//            }
//            //endregion
//        }
//    }
//
//    public void loadInviteProfileByGlide(final ImageView imageView, final JsonInvite jsonInvite, ImageType imageType, final boolean NoPhotoInverse, final LoadProfileCallback callback) {
//        //region Set Variable values
//        final String imgUrl = jsonInvite.getUserProfilePic();
//        final String displayName = jsonInvite.getUserFullName();
//        //endregion
//
//        final Drawable drawable = ImageUtility.setupUsersImageIfNotPic(displayName, imageType, NoPhotoInverse);
//
//        if (imgUrl != null && !imgUrl.equals("")) {
//            //region if (isMeuContact)
//            if (imageType == ImageType.Round) {
//                //region ImageType.Round
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
////                                callback.onLoadProfilePicture(true);
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
////                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//                //endregion
//            } else {
//                //region ImageType.Rect
//                LogApp.w("Picasso", "1 --- jsonContact= " + imgUrl);
//
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .bitmapTransform(new BlurTransformation(MyApplication.getInstance(), 10))
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//
//                                LogApp.e("Picasso", "2 --- jsonContact= " + imgUrl);
//
////                                //region Load original image
//                                Glide.with(MyApplication.getInstance())
//                                        .load(imgUrl)
//                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(resource.getCurrent())
//                                        .error(drawable)
//                                        .into(new GlideDrawableImageViewTarget(imageView) {
//                                            @Override
//                                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                                super.onResourceReady(resource, animation);
//                                                LogApp.d("Picasso", "3 --- onResourceReady= ");
//
//                                                callback.onLoadProfilePicture(true);
//                                            }
//
//                                            @Override
//                                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                                super.onLoadFailed(e, errorDrawable);
//                                                LogApp.d("Picasso", "4 --- onResourceReady= ");
//
//                                                callback.onLoadProfilePicture(false);
//                                            }
//                                        });
////                                //endregion
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//                //endregion
//            }
//            //endregion
//        } else {
//            //region if (! isMeuContact)
//            if (imageType == ImageType.Round) {
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(imageView);
//            } else {
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(imageView);
//            }
//            //endregion
//        }
//    }
//
//    public void loadMyProfileByGlide(Activity activity, final ImageView imageView, final ImageSource imageSource, ImageType imageType, final boolean getProfile, final boolean NoPhotoInverse, boolean isViewInEditProfile, final LoadProfileCallback callback) {
//        //region Set Variable values
//        String imgUrl = "";
//        String displayName = "";
//
//        if (MyAccount.getInstance() != null) {
//            String firstName = (MyAccount.getInstance(activity).getFirstName() != null) ? MyAccount.getInstance(activity).getFirstName() : "";
//            String lastName = (MyAccount.getInstance(activity).getLastName() != null) ? MyAccount.getInstance(activity).getLastName() : "";
//
//            displayName = String.format("%s %s", firstName, lastName).trim();
//
//            if (imageSource == ImageSource.Thumbnail) {
//                imgUrl = (MyAccount.getInstance(activity).getThumbnailImagePath() != null) ? MyAccount.getInstance(activity).getThumbnailImagePath() : null;
//            } else {
//                imgUrl = (MyAccount.getInstance(activity).getThumbnailImagePath() != null) ? MyAccount.getInstance(activity).getOriginalImagePath() : null;
//            }
//        }
//
//        if (imgUrl != null)
//            if (imgUrl.trim().equals(""))
//                imgUrl = null;
//
////        final String finalImgUrl = imgUrl;
//        //endregion
//
//        LogApp.w("Glide", "displayName= " + displayName + " , URL= " + imgUrl);
//        LogApp.e("Glide", "--------------------------------------------------------------------");
//
//        final Drawable drawable = ImageUtility.setupUsersImageIfNotPic(displayName, imageType, NoPhotoInverse);
//
//        if (imgUrl != null && !imgUrl.equals("")) {
//            //region if (imgUrl != null)
//
//            if (imageType == ImageType.Round) {
//                //region ImageType.Round
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
////                                resource.getCurrent()
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                            }
//                        });
//                //endregion
//            } else {
//                //region ImageType.Rect
//                if (isViewInEditProfile) {
//                    final String imageLocalPath = (MyAccount.getInstance(activity).getUserImagePath() != null) ? MyAccount.getInstance(activity).getUserImagePath() : null;
//                    final String imageUrl = (MyAccount.getInstance(activity).getOriginalImagePath() != null) ? MyAccount.getInstance(activity).getOriginalImagePath() : null;
//                    String imagePath = null;
//                    if ((imageLocalPath != null) && (!imageLocalPath.equals("")) && FileUtility.exist(imageLocalPath)) {
//                        imgUrl = imageLocalPath;
//                    } else if ((imageUrl != null) && (!imageUrl.equals(""))) {
//                        imgUrl = imageLocalPath;
//                    } else {
//                        imgUrl = null;
//                    }
//
//                    Glide.with(MyApplication.getInstance())
//                            .load(imgUrl)
//                            .diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .error(R.drawable.ic_photo_camera_gray_48dp)
//                            .into(new GlideDrawableImageViewTarget(imageView) {
//                                @Override
//                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                    super.onResourceReady(resource, animation);
//                                    callback.onLoadProfilePicture(true);
//                                }
//
//                                @Override
//                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                    super.onLoadFailed(e, errorDrawable);
//                                    callback.onLoadProfilePicture(false);
//                                }
//                            });
//                } else {
//                    Glide.with(MyApplication.getInstance())
//                            .load(imgUrl)
//                            .diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .placeholder(drawable)
//                            .error(drawable)
//                            .into(imageView);
//                }
//                //endregion
//            }
//            //endregion
//        } else {
//            //region if (imgUrl == null)
//            if (imageType == ImageType.Round) {
//                //region Glide
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(imageView);
//                //endregion
//            } else {
//                //region Glide
//                Glide.with(MyApplication.getInstance())
//                        .load(imgUrl)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(drawable)
//                        .error(drawable)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                            }
//                        });
//                //endregion
//            }
//            //endregion
//        }
//    }
//
//    public void loadMyProfileByGlide(Activity activity, final ImageView imageView, ImageType imageType, int resourceDrawableError, final LoadProfileCallback callback) {
//        //region Set Variable values
//        final String TAG = "LoadProfileImage";
//
//        String displayName = "";
//        String firstName = "";
//        String lastName = "";
//
//        String imagePath = null;
//
//        String imageLocalPath = null;
//        String imageThumbnailUrl = null;
//        String imageOriginalUrl = null;
//
//        if (MyAccount.getInstance() != null) {
//            firstName = (MyAccount.getInstance(activity).getFirstName() != null) ? MyAccount.getInstance(activity).getFirstName() : "";
//            lastName = (MyAccount.getInstance(activity).getLastName() != null) ? MyAccount.getInstance(activity).getLastName() : "";
//
//            displayName = String.format("%s %s", firstName, lastName).trim();
//
//            imageLocalPath = (MyAccount.getInstance(activity).getUserImagePath() != null) ? MyAccount.getInstance(activity).getUserImagePath() : null;
//            imageThumbnailUrl = (MyAccount.getInstance(activity).getThumbnailImagePath() != null) ? MyAccount.getInstance(activity).getThumbnailImagePath() : null;
//            imageOriginalUrl = (MyAccount.getInstance(activity).getOriginalImagePath() != null) ? MyAccount.getInstance(activity).getOriginalImagePath() : null;
//
//            if ((imageLocalPath != null) && (!imageLocalPath.equals("")) && FileUtility.exist(imageLocalPath)) {
//                imagePath = "file://" + imageLocalPath;
//            } else if ((imageThumbnailUrl != null) && (!imageThumbnailUrl.equals(""))) {
//                imagePath = imageThumbnailUrl;
//            } else {
//                imagePath = null;
//            }
//        }
//        //endregion
//
//        LogApp.w(TAG, "displayName= " + displayName + " , URL= " + imagePath);
//        LogApp.e(TAG, "--------------------------------------------------------------------");
//
//        imageView.setScaleType(ImageView.ScaleType.CENTER);
//
//        if (imageType == ImageType.Round) {
//            //region ImageType.Round
//            Glide.with(MyApplication.getInstance())
//                    .load(imagePath)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .bitmapTransform(new CropCircleTransformation(MyApplication.getInstance()))
////                    .placeholder(resourceDrawableError)
//                    .error(resourceDrawableError)
//                    .into(new GlideDrawableImageViewTarget(imageView) {
//                        @Override
//                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                            super.onResourceReady(resource, animation);
//                            callback.onLoadProfilePicture(true);
//                        }
//
//                        @Override
//                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                            super.onLoadFailed(e, errorDrawable);
//                            callback.onLoadProfilePicture(false);
//                        }
//                    });
//            //endregion
//        } else {
//            //region ImageType.Rect
//            if (imagePath != null && !imagePath.startsWith("file")) {
//                //region Load in server
//                final String finalImageOriginalUrl = imageOriginalUrl;
//
//                Glide.with(MyApplication.getInstance())
//                        .load(imagePath)
//                        .bitmapTransform(new BlurTransformation(MyApplication.getInstance(), 25))
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .error(resourceDrawableError)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//
//                                //region Download original image
//                                Glide.with(MyApplication.getInstance())
//                                        .load(finalImageOriginalUrl)
//                                        .downloadOnly(new SimpleTarget<File>() {
//                                            @Override
//                                            public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
//                                                try {
//                                                    String mCurrentPhotoPath = new AddressFactory().getProfileAddress();
//
//                                                    FileUtility.CopyByChannel(resource.getAbsolutePath(), mCurrentPhotoPath);
//
//                                                    MyAccount.getInstance().setUserImagePath(mCurrentPhotoPath);
//                                                    MyAccount.getInstance().saveUserData();
//
//                                                    LogApp.w(TAG, "Image loaded size= " + resource.length() + " , path= " + resource.getPath());
//
//                                                    callback.onLoadProfilePicture(true);
//                                                } catch (IOException e) {
//                                                    e.printStackTrace();
//                                                    LogApp.e(TAG, "Image loaded size= " + e.getMessage());
//
//                                                    callback.onLoadProfilePicture(false);
//                                                }
//                                            }
//                                        });
//                                //endregion
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//
//                //endregion
//            } else {
//                //region Load in local
//                Glide.with(MyApplication.getInstance())
//                        .load(imagePath)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .error(resourceDrawableError)
//                        .into(new GlideDrawableImageViewTarget(imageView) {
//                            @Override
//                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                                super.onResourceReady(resource, animation);
//                                callback.onLoadProfilePicture(true);
//                            }
//
//                            @Override
//                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                                super.onLoadFailed(e, errorDrawable);
//                                callback.onLoadProfilePicture(false);
//                            }
//                        });
//                //endregion
//            }
//            //endregion
//        }
//    }
//endregion
}
