package com.example.basearchitecture.base.BestUtility;

import android.widget.EditText;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtils {
    private static final int DefaultCountryCode = 98;

    public static Boolean IsValidNationalCode(String nationalCode) throws Exception {
        if (StringUtils.isEmpty(nationalCode)) {
            throw new Exception("لطفا کد ملی را وارد نمایید.");
        }

        if (nationalCode.length() != 10) {
            throw new Exception("طول کد ملی باید ده کاراکتر باشد.");
        }

        if (!isNumber(nationalCode)) {
            throw new Exception("کد ملی تشکیل شده از ده رقم عددی می‌باشد؛ لطفا کد ملی را صحیح وارد نمایید.");
        }

        //در صورتی که رقم‌های کد ملی وارد شده یکسان باشد
        String[] allDigitEqual = {"0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999"};
        boolean isFound = Arrays.binarySearch(allDigitEqual, nationalCode) > 0;
        if (isFound)
            throw new Exception("کد ملی وارد شده معتبر نیست؛ لطفا کد ملی را صحیح وارد نمایید.");

        //عملیات شرح داده شده در بالا
        char[] Code = nationalCode.toCharArray();
        long c, n, r;

        c = Long.parseLong(String.valueOf(Code[9]));
        n = (Long.parseLong(String.valueOf(Code[0])) * 10 +
                Long.parseLong(String.valueOf(Code[1])) * 9 +
                Long.parseLong(String.valueOf(Code[2])) * 8 +
                Long.parseLong(String.valueOf(Code[3])) * 7 +
                Long.parseLong(String.valueOf(Code[4])) * 6 +
                Long.parseLong(String.valueOf(Code[5])) * 5 +
                Long.parseLong(String.valueOf(Code[6])) * 4 +
                Long.parseLong(String.valueOf(Code[7])) * 3 +
                Long.parseLong(String.valueOf(Code[8])) * 2);

        r = n - (int) (n / 11) * 11;

        if ((r == 0 && r == c) || (r == 1 && c == 1) || (r > 1 && c == 11 - r)) {
            return true;
        } else {
            throw new Exception("کد ملی وارد شده معتبر نیست؛ لطفا کد ملی را صحیح وارد نمایید.");
        }
    }

    public static Boolean isValidPhoneNumber(int _countryCode, String _nationalNumber) {
        boolean isValidNationalNumber = false;

        try {
            if (!_nationalNumber.startsWith("0") && android.util.Patterns.PHONE.matcher(_nationalNumber).matches()) {
               /* PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                Phonenumber.PhoneNumber meuPhoneNumber = new Phonenumber.PhoneNumber()
                        .setCountryCode(_countryCode)
                        .setNationalNumber(Long.valueOf(_nationalNumber));

                isValidNationalNumber = phoneUtil.isValidNumber(meuPhoneNumber);*/
            }
        } catch (Exception e) {
            isValidNationalNumber = false;
        }

        return isValidNationalNumber;
    }

    public static Boolean isValidPhoneNumber(String PhoneNumber) {
        return isValidPhoneNumber(DefaultCountryCode, PhoneNumber.trim().substring(1));
    }

    public static boolean isValidEmail(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static Boolean isValidPassword(String _password) {
//        final String PASSWORD_REGEXP = "^"
//                + "(?=.*\\d)"
//                + "(?=.*[a-z])"
//                + "(?=.*[A-Z])"
//                + "(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?])"
//                + "."
//                + "{6,18}"
//                + "$";

        final String PASSWORD_REGEXP = "^"
                + "(?=.*\\d)"
                + "."
                + "{6,18}"
                + "$";

        Pattern pattern = Pattern.compile(PASSWORD_REGEXP);
        Matcher matcher;

        matcher = pattern.matcher(_password);

        LogApp.w("isValidPassword", String.valueOf(matcher.matches()));

        return matcher.matches();
    }

    public static boolean isNumber(String string) {
        return string.matches("^\\d+$");
    }

    public static boolean isEmpty(CharSequence str) {
        return isNull(str) || str.length() == 0;
    }

    public static boolean isEmpty(Object[] os) {
        return isNull(os) || os.length == 0;
    }

    public static boolean isEmpty(Number[] os) {
        return isNull(os) || os.length == 0;
    }

    public static boolean isEmpty(Collection<?> l) {
        return isNull(l) || l.isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> m) {
        return isNull(m) || m.isEmpty();
    }

    public static boolean isNullOrEmpty(String s) {
        return isNull(s) || s.isEmpty();
    }

    public static boolean isNullOrEmpty(EditText s) {
        return isNull(s);
    }

    public static boolean isNull(Object o) {
        return o == null;
    }
}