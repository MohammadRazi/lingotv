package com.example.basearchitecture.base.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;
import com.example.basearchitecture.R;
import com.example.basearchitecture.base.BestUtility.FontUtils;

/**
 * Created by Ali on 13/02/2018.
 */
public class CustomEditText extends AppCompatEditText {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
    }

    //-------------------------------------------------------------------------------------------
    void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.CustomEditText);

            setDrawable(context, typedArray);
            setTypeface(typedArray);

            typedArray.recycle();
        }
    }

    private void setDrawable(Context context, TypedArray typedArray) {
        Drawable drawableLeft = null;
        Drawable drawableRight = null;
        Drawable drawableBottom = null;
        Drawable drawableTop = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawableLeft = typedArray.getDrawable(R.styleable.CustomEditText_drawableLeftCompat);
            drawableRight = typedArray.getDrawable(R.styleable.CustomEditText_drawableRightCompat);
            drawableBottom = typedArray.getDrawable(R.styleable.CustomEditText_drawableBottomCompat);
            drawableTop = typedArray.getDrawable(R.styleable.CustomEditText_drawableTopCompat);
        } else {
            final int drawableLeftId = typedArray.getResourceId(R.styleable.CustomEditText_drawableLeftCompat, -1);
            final int drawableRightId = typedArray.getResourceId(R.styleable.CustomEditText_drawableRightCompat, -1);
            final int drawableBottomId = typedArray.getResourceId(R.styleable.CustomEditText_drawableBottomCompat, -1);
            final int drawableTopId = typedArray.getResourceId(R.styleable.CustomEditText_drawableTopCompat, -1);

            if (drawableLeftId != -1)
                drawableLeft = AppCompatResources.getDrawable(context, drawableLeftId);
            if (drawableRightId != -1)
                drawableRight = AppCompatResources.getDrawable(context, drawableRightId);
            if (drawableBottomId != -1)
                drawableBottom = AppCompatResources.getDrawable(context, drawableBottomId);
            if (drawableTopId != -1)
                drawableTop = AppCompatResources.getDrawable(context, drawableTopId);
        }

        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
        setCompoundDrawablePadding(3);
//            setHeight(100);

    }

    private void setTypeface(TypedArray typedArray) {
        int typeface;
        if (typedArray != null) {
            typeface = typedArray.getInt(R.styleable.CustomEditText_typeface, Fonts.CONSOLAS);
            setTypeface(getCustomTypeface(typeface));
        }
    }

    //-------------------------------------------------------------------------------------------
    public void setCustomTypeface(int typeface) {
        setTypeface(getCustomTypeface(typeface));
    }

    public Typeface getCustomTypeface(Context context, int typeface) {
        switch (typeface) {
            case Fonts.IRAN_YEKAN:
                return FontUtils.F_IranYekan().getTypeface();
            case Fonts.CONSOLAS:
                return FontUtils.F_Consolas().getTypeface();
            default:
                return FontUtils.Default().getTypeface();
        }
    }

    private Typeface getCustomTypeface(int typeface) {
        return getCustomTypeface(getContext(), typeface);
    }

    public static class Fonts {
        static final int IRAN_YEKAN = 0;
        static final int CONSOLAS = 1;
    }
}
