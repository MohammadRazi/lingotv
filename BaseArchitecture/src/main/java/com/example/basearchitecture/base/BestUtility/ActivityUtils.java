package com.example.basearchitecture.base.BestUtility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Pair;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.example.basearchitecture.R;


public class ActivityUtils {
    private static ActivityUtils ourInstance = new ActivityUtils();

    private ActivityUtils() {
    }

    public static ActivityUtils getInstance() {
        if (ourInstance == null)
            ourInstance = new ActivityUtils();

        return ourInstance;
    }

    public void startActivityWithTransition(View view, Activity activity, Class dest, Bundle bundle) {
        Intent intent = new Intent(activity, dest);
        intent.putExtra("data", bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewCompat.setTransitionName(view, "item");

            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(
                    activity,
                    Pair.create(view, "item"));

            activity.startActivity(intent, activityOptions.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    public void startActivityWithTransition(Activity activity, Class dest, View v) {
        Intent intent = new Intent(activity, dest);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewCompat.setTransitionName(v, "item");

            ActivityOptions activityOptions = null;
            activityOptions = ActivityOptions.makeSceneTransitionAnimation(
                    activity,
                    Pair.create(v, "item"));

            activity.startActivity(intent, activityOptions.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    public void startActivity(Activity activity, Class dest) {
        Intent intent = new Intent(activity, dest);
        activity.startActivity(intent);
    }

    public void startActivity(Activity activity, Class dest, Bundle bundle) {
        Intent intent = new Intent(activity, dest);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public void startActivity(Activity activity, Class dest, boolean showWithAnim) {
        Intent intent = new Intent(activity, dest);

        if (showWithAnim) {
            Bundle bundle = ActivityOptions.makeCustomAnimation(activity, R.anim._in, R.anim._out).toBundle();
            activity.startActivity(intent, bundle);
        } else {
            activity.startActivity(intent);
        }
    }

    public void startActivity(Context context, Class className, Bundle bundle, boolean topActivity, boolean newTask) {
        Intent intent = new Intent(context, className);

        if (topActivity) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else if (newTask) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        if (bundle != null) {
            context.startActivity(intent, bundle);
        } else {
            context.startActivity(intent);
        }
    }

    public void startActivityWithFinishAffinity(Activity activity, Class dest) {
        Intent intent = new Intent(activity, dest);
        activity.startActivity(intent);
        activity.finishAffinity();
    }

    public void startActivityWithFinishAffinity(Activity activity, Class dest, Bundle bundle) {
        Intent intent = new Intent(activity, dest);
        intent.putExtras(bundle);

        activity.startActivity(intent);
        activity.finishAffinity();
    }

//    public void startActivity(Context context, Class className, int flag) {
//        Intent intent = new Intent(context, className);
//
//        if (flag == 1) {
//            intent.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
//        } else {
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        }
//
//        context.startActivity(intent);
//    }

    public void setPendingTransition(Activity activity, ActionActivity actionActivity) {
        if (actionActivity == ActionActivity.Open) {
            activity.overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);
        } else {
            activity.overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);
        }
    }

    public void setPendingTransition(Activity activity, int animStart, int animEnd) {
        try {
            activity.overridePendingTransition(animStart, animEnd);
        } catch (Exception e) {
            LogApp.e("setPendingTransition", e.getMessage());
        }
    }

    public void setRTL(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    public void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public void hideKeyboard(Dialog dialog) {
        try {
            View view = dialog.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) dialog.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        } catch (Exception ignored) {
        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass, final Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public Display getDisplaySize() {
        return ((WindowManager) UsefulUtility.getInstance().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    }

//    public void initActionBar(AppCompatActivity activity, int resId, String title) {
//        Toolbar toolbar = activity.findViewById(resId);
//
//        activity.setSupportActionBar(toolbar);
//        ActionBar actionBar = activity.getSupportActionBar();
//
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(false);
//
//            TextView toolbar_Title = toolbar.findViewById(R.id.toolbar_Title);
//            toolbar_Title.setText(title);
//
//            FontUtils.setFont(toolbar_Title);
//        }
//    }

    public void initActionBar(AppCompatActivity activity, int resId) {
        Toolbar toolbar = activity.findViewById(resId);

        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    public enum ActionActivity {
        Open("open"),
        close("close");

        private final String value;

        ActionActivity(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}