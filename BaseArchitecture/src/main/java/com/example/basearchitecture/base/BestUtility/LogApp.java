package com.example.basearchitecture.base.BestUtility;

import android.os.Bundle;
import android.util.Log;
import com.example.basearchitecture.BuildConfig;

public class LogApp {
    public static String foo() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        return stackTraceElements[3].getMethodName();
//
//        return new Object() {
//        }.getClass().getName();
    }

    public static int v(String appTag, String message) {
        if (BuildConfig.DEBUG)
            return Log.v("LogApp_" + getTag() + "_" + appTag, message != null ? message : "null");
        else
            return -1;
    }

    public static int i(String appTag, String message) {
        if (BuildConfig.DEBUG)
            return Log.i("LogApp_" + getTag() + "_" + appTag, message != null ? message : "null");
        else
            return -1;
    }

    public static int e(String appTag, String message) {
        if (BuildConfig.DEBUG)
            return Log.e("LogApp_" + getTag() + "_" + appTag, message != null ? message : "null");
        else
            return -1;
    }

    public static int w(String appTag, String message) {
        if (BuildConfig.DEBUG)
            return Log.w("LogApp_" + getTag() + "_" + appTag, message != null ? message : "null");
        else
            return -1;
    }

    public static int d(String appTag, String message) {
        if (BuildConfig.DEBUG)
            return Log.d("LogApp_" + getTag() + "_" + appTag, message != null ? message : "null");
        else
            return -1;
    }

    public static int v(String message) {
        if (BuildConfig.DEBUG)
            return v("", message);
        else
            return -1;
    }

    public static int i(String message) {
        if (BuildConfig.DEBUG)
            return i("", message);
        else
            return -1;
    }

    public static int e(String message) {
        if (BuildConfig.DEBUG)
            return e("", message);
        else
            return -1;
    }

    public static int w(String message) {
        if (BuildConfig.DEBUG)
            return w("", message);
        else
            return -1;
    }

    public static int d(String message) {
        if (BuildConfig.DEBUG)
            return d("", message);
        else
            return -1;
    }

    //------------------------------------------------------------------------------------------
    public void printBundle(Bundle bundle) {
        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            w("bundle=>" + String.format("%s %s (%s)", key, value.toString(), value.getClass().getName()));
        }
    }

    public static void printCurrentStackTrace(String tag) {
        try {
            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                i("LogApp_" + tag, ste.toString());
            }
        } catch (Exception ex) {
            e("printCurrentStackTrace", ex.getMessage());
        }
    }

    public static void trace(StackTraceElement e[]) {
        boolean doNext = false;
        for (StackTraceElement s : e) {
            if (doNext) {
                System.out.println(s.getMethodName());
                return;
            }
            doNext = s.getMethodName().equals("getStackTrace");
        }
    }

    public static void setUncaughtExceptionHandler() {
        // Setup handler for uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
    }

    private static void handleUncaughtException(Thread thread, Throwable ex) {
        e("handleUncaughtException", "handleUncaughtException ERROR : " + ex);
        ex.printStackTrace(); // not all Android versions will print the stack trace automatically

        System.exit(1); // kill off the crashed app
    }

    //-------------------------------------------------------------------------------------------
    public static String getTag() {
//        return getClassName() + "_" + getMethodName();
        return "Insurance";
    }

    public static String getClassName(Class c) {
        String fullClassName = c.getName();
        String simpleClassName = fullClassName.substring(fullClassName.lastIndexOf('.') + 1);
        simpleClassName = simpleClassName.substring(0, simpleClassName.length() - 2);

        return simpleClassName;
    }

    public static String getClassName() {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[6];
        String s = ste.toString();
//        s = s.substring(s.indexOf("(") + 1, s.lastIndexOf("."));
        s = s.substring(s.indexOf("(") + 1);

        return s;
    }

    public static String getMethodName() {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[6];
        String s = ste.toString();
        s = s.substring(0, s.lastIndexOf("("));
        s = s.substring(s.lastIndexOf(".") + 1);

        return s;
    }
}