package com.example.basearchitecture.base.BestUtility;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class DispatchThread {
    private static ThreadPoolExecutor poolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
    private static DispatchThread instance = null;
    private static Handler uiHandler;
    private Dictionary<String, Handler> lstGroupedActions = new Hashtable<>();

    public static DispatchThread getInstance() {
        if (instance == null) {
            instance = new DispatchThread();
            uiHandler = new Handler(UsefulUtility.getInstance().getMainLooper());
        }
        return instance;
    }

    public void postRunnable(Runnable runnable) {
        if (!poolExecutor.isShutdown() && poolExecutor.getActiveCount() != poolExecutor.getMaximumPoolSize()) {
            poolExecutor.submit(runnable);
        } else {
            new Thread(runnable).start();
        }
    }

    public void postRunnableDelay(Runnable runnable, long delay) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(runnable, delay);
    }

    public void postRunnableOnUI(Runnable runnable) {
        uiHandler.post(runnable);
    }

    public void postRunnableDelayOnUI(Runnable runnable, long delay) {
        uiHandler.postDelayed(runnable, delay);
    }

    public void postRunnableDelayOnUI(Context context, String group, long delay, Runnable runnable) {
        Handler handler = lstGroupedActions.get(group);
        if (handler == null) {
            handler = new Handler(context.getMainLooper());
            lstGroupedActions.put(group, handler);
        }

        handler.postDelayed(runnable, delay);
    }

    public void postSingleRunnableDelayOnUI(Context context, String group, long delay, Runnable runnable) {
        Handler handler = lstGroupedActions.get(group);
        if (handler == null) {
            handler = new Handler(context.getMainLooper());
            lstGroupedActions.put(group, handler);
        }
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(runnable, delay);
    }

    public void clearDelayOnUI(String group) {

        Handler handler = lstGroupedActions.get(group);
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        lstGroupedActions.remove(group);
    }
}

