package com.example.basearchitecture.base.component;

import android.content.Context;
import android.content.res.TypedArray;
import com.google.android.material.textfield.TextInputLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.basearchitecture.R;

import java.lang.reflect.Field;

/**
 * Created by Ali on 13/02/2018.
 */
public class CustomTextInputLayout extends TextInputLayout {

    boolean isRtl;

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomTextInputLayout, 0, 0);
        try {
            isRtl = ta.getBoolean(R.styleable.CustomTextInputLayout_isRtl, true);
        } finally {
            ta.recycle();
        }
    }

    public boolean isRtl() {
        return isRtl;
    }

    public void setRtl(boolean rtl) {
        isRtl = rtl;
    }

    @Override
    public void setErrorEnabled(boolean enabled) {
        super.setErrorEnabled(enabled);

        if (!enabled) {
            return;
        }

        try {
            Field errorViewField = TextInputLayout.class.getDeclaredField("mErrorView");
            errorViewField.setAccessible(true);
            TextView errorView = (TextView) errorViewField.get(this);
            if (errorView != null) {
                if (isRtl) {
                    errorView.setGravity(Gravity.RIGHT);
                } else {
                    errorView.setGravity(Gravity.LEFT);
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.END;
                errorView.setLayoutParams(params);
            }
        } catch (Exception e) {
            // At least log what went wrong
            e.printStackTrace();
        }
    }
}
