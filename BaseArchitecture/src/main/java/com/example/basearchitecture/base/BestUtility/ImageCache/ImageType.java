package com.example.basearchitecture.base.BestUtility.ImageCache;

/**
 * Created by Ali on 25/05/2016.
 */
public enum ImageType {
    Rect(1),
    Round(2);

    private final int value;

    ImageType(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }
}