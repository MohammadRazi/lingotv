package com.example.basearchitecture.base.BestUtility;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.example.basearchitecture.R;

public class AnimUtils {
    public static void showView(final Activity activity, final View view, final long delay, final int anim) {
        final Animation animation = AnimationUtils.loadAnimation(activity, anim);

//            Thread thread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
        view.setAnimation(animation);
//                        }
//                    }, delay);
//                }
//            });
//
//            thread.run();
    }

    public static void showView(final Activity activity, final ViewGroup view, final long delay, final int anim) {
        final Animation animation = AnimationUtils.loadAnimation(activity, anim);


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setAnimation(animation);
                    }
                }, delay);
            }
        });

        thread.run();


        ViewGroup viewGroup = view;
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            viewGroup.getChildAt(i).setAnimation(animation);
        }

    }

    public static void changeColor(Context context, final View view, int colorFrom, int colorTo) {
        ValueAnimator colorAnimation_off = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
        ValueAnimator colorAnimation_on = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);

        colorAnimation_off.setDuration(250);
        colorAnimation_off.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (view instanceof ImageView) {
                    ((ImageView) view).setColorFilter((int) animator.getAnimatedValue());
                }
            }
        });
        colorAnimation_off.start();
    }

    public static void shake(Activity activity, final View view) {
        if (view != null) {
            Animation shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.shake);
            view.startAnimation(shakeAnimation);
        }
    }

    public static void shake(Activity activity, final View view, boolean isVibrate, final callback _callback) {
        if (view != null) {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    _callback.done();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            if (isVibrate) {
                SystemUtils.vibrate();
            }

            view.startAnimation(animation);
        }
    }

    private void ClickOnImageView(final Activity activity, final ImageView imageView, boolean isVisible) {
        if (isVisible) {
            if (!imageView.isEnabled()) {
                Animation mAnim = AnimationUtils.loadAnimation(activity, R.anim.img_hide);
                mAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            imageView.setBackgroundTintList(ColorStateList.valueOf(UIUtil.getColor(R.color.m_dialog_Text)));
                        }
                        imageView.setColorFilter(UIUtil.getColor(android.R.color.white));

                        Animation mAnim = AnimationUtils.loadAnimation(activity, R.anim.img_show);
                        imageView.startAnimation(mAnim);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                imageView.startAnimation(mAnim);
            }
        } else {
            if (imageView.isEnabled()) {
                Animation mAnim = AnimationUtils.loadAnimation(activity, R.anim.img_hide);
                mAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            imageView.setBackgroundTintList(ColorStateList.valueOf(UIUtil.getColor(android.R.color.darker_gray)));
                        }
                        imageView.setColorFilter(UIUtil.getColor(android.R.color.darker_gray));

                        Animation mAnim = AnimationUtils.loadAnimation(activity, R.anim.img_show);
                        imageView.startAnimation(mAnim);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                imageView.startAnimation(mAnim);
            }
        }
        imageView.setEnabled(isVisible);
    }

    public interface callback {
        void done();
    }

//        public void ClickOnImageView(final Activity activity, final ImageView imageView) {
//            Animation mAnim = AnimationUtils.loadAnimation(activity, R.anim.img_hide);
//            mAnim.setAnimationListener(new Animation.AnimationListener() {
//                @Override
//                public void onAnimationStart(Animation animation) {
//
//                }
//
//                @Override
//                public void onAnimationEnd(Animation animation) {
//                    LogApp.w("ClickOnImageView", "onAnimationEnd");
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        imageView.setBackground(UIUtil.getDrawable(R.drawable.shape_selecteditem_dot));
////                    imageView.animate()
////                            .scaleX(1.5f)
////                            .scaleY(1.5f)
////                            .setStartDelay(10)
////                            .setInterpolator(new FastOutSlowInInterpolator())
////                            .setDuration(450)
////                            .start();
//                    }
//
////                imageView.setColorFilter(Utility.getInstance().getColor(R.color.Yellow));
//
//                    Animation mAnim = AnimationUtils.loadAnimation(activity, R.anim.img_show);
//                    imageView.startAnimation(mAnim);
//                }
//
//                @Override
//                public void onAnimationRepeat(Animation animation) {
//
//                }
//            });
//            imageView.startAnimation(mAnim);
//        }

    public static class BounceInterpolator implements android.view.animation.Interpolator {
        private double mAmplitude = 0.2;
        private double mFrequency = 20;

        public BounceInterpolator() {
        }

        public BounceInterpolator(double amplitude, double frequency) {
            mAmplitude = amplitude;
            mFrequency = frequency;
        }

        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) * Math.cos(mFrequency * time) + 1);
        }

        public void didTapButton(Activity activity, View view, final callback mCallback) {
            final Animation myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce_show_view);

            BounceInterpolator interpolator = new BounceInterpolator(mAmplitude, mFrequency);
            myAnim.setInterpolator(interpolator);

            myAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (mCallback != null)
                        mCallback.done();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            view.startAnimation(myAnim);
        }

        public void didTapButton(Activity activity, View view, int anim) {
            final Animation myAnim = AnimationUtils.loadAnimation(activity, anim);

            BounceInterpolator interpolator = new BounceInterpolator(mAmplitude, mFrequency);
            myAnim.setInterpolator(interpolator);

            view.startAnimation(myAnim);
        }

        public void clickWithAnim(final Activity activity, final View view, callback mCallback) {
//                mAmplitude = 0.2;
//                mFrequency = 20;

            didTapButton(activity, view, mCallback);
        }

        public void showView(final Activity activity, final View view, long delay, final callback callback) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    didTapButton(activity, view, callback);
                }
            }, delay);
        }

        public void showView(final Activity activity, final View view, long delay, final int anim) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    didTapButton(activity, view, anim);
                }
            }, delay);
        }
    }

    public static class SlideAnimationUtil {
        public static void slideInFromLeft(Context context, View view) {
            runSimpleAnimation(context, view, R.anim.activity_slide_in_left);
        }

        public static void slideOutToLeft(Context context, View view) {
            runSimpleAnimation(context, view, R.anim.activity_slide_out_left);
        }

        public static void slideInFromRight(Context context, View view) {
            runSimpleAnimation(context, view, R.anim.activity_slide_in_right);
        }

        public static void slideOutToRight(Context context, View view) {
            runSimpleAnimation(context, view, R.anim.activity_slide_out_right);
        }

        private static void runSimpleAnimation(Context context, View view, int animationId) {
            view.startAnimation(AnimationUtils.loadAnimation(
                    context, animationId
            ));
        }
    }

}
