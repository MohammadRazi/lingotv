package com.example.basearchitecture.base.BestUtility.ImageCache;

/**
 * Created by Ali on 25/05/2016.
 */
public enum ImageSource {
    Thumbnail(1),
    Original(2);

    private final int value;

    ImageSource(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }
}