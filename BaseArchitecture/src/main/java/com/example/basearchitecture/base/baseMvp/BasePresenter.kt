package com.example.user.nlingotv.base

import android.content.Context
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V> {
    protected var view: V? = null
    protected var context: Context? = null
    protected var compositeDisposable: CompositeDisposable? = CompositeDisposable()


    internal fun attachView(view: V, context: Context) {
        this.view = view
        this.context = context
    }

    internal fun deAttachView() {
        this.view = null
    }

     internal fun unSubscribe() {
        if (compositeDisposable !=null && !compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
        }
    }

}