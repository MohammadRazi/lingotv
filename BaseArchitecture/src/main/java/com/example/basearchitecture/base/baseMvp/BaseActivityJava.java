package com.example.basearchitecture.base.baseMvp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.basearchitecture.R;
import com.example.basearchitecture.base.BestUtility.ActivityUtils;
import com.example.basearchitecture.base.BestUtility.AlertDialogUtils;
import com.example.basearchitecture.base.BestUtility.SystemUtils;
import com.example.basearchitecture.base.EventBusModel;
import com.example.basearchitecture.base.component.CustomToast;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class BaseActivityJava extends AppCompatActivity implements IBaseView {
    protected Unbinder unbinder;
    protected AppCompatActivity activity;
    protected Context context;
    protected ProgressDialog progressDialog;

    protected abstract void onEvent(EventBusModel event);

    protected abstract void initBeforeView();

    protected abstract int getContentViewId();

    protected abstract void initNavigation();

    protected abstract void initToolbar();

    protected abstract void initViews();

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventBus(EventBusModel event) {
        onEvent(event);
    }

    //region Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBeforeView();

        setContentView(getContentViewId());

        activity = this;
        context = this;

        unbinder = ButterKnife.bind(this);

        ActivityUtils.getInstance().hideKeyboard(activity);
        ActivityUtils.getInstance().setRTL(activity);

        progressDialog = AlertDialogUtils.generateProgressDialog(activity, "در حال ارتباط با سرور...");

        initNavigation();
        initToolbar();
        initViews();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

//    @Override
//    protected void onPause() {
//        EventBus.getDefault().unregister(this);
//        super.onPause();
//    }
//
//
//
//    @Override
//    protected void onResume() {
//        EventBus.getDefault().register(this);
//        super.onResume();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgress();
        unbinder.unbind();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ActivityUtils.getInstance().hideKeyboard(activity);
        overridePendingTransitionExit();
    }
    //endregion

    //region IMasterActivity
    @Override
    public void networkConnectionError() {
        AlertDialogUtils.ShowAlertDialog(activity, "لطفا ارتباط با اینترنت را بررسی کرده و دوباره تلاش کنید.");
    }

    @Override
    public void unableToConnect() {
        CustomToast.makeText(activity, "ارتباط با سرور برقرار نشد!", CustomToast.ERROR);
    }

    @Override
    public void showMessage(String message) {
        AlertDialogUtils.ShowAlertDialog(activity, message);
    }

    @Override
    public void showError(String errorMessage) {
        CustomToast.makeText(activity, errorMessage, CustomToast.ERROR);
    }

    @Override
    public void showProgress() {
        AlertDialogUtils.setProgressDialogStatus(true, progressDialog);
    }

    @Override
    public void hideProgress() {
        AlertDialogUtils.setProgressDialogStatus(false, progressDialog);
    }

    @Override
    public void vibrate() {
        SystemUtils.vibrate();
    }

    @Override
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void setProcess(int progress) {
        progressDialog.setMessage(progress + "%");
        showProgress();
    }

    //endregion

    //region Transition Methods
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);
    }
    //endregion
}
