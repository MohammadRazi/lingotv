package com.example.basearchitecture.base;

public class EventBusModel extends BaseModel {
    private int resultCode;
    private Object resultValue;

    public EventBusModel(int resultCode, Object resultValue) {
        this.resultCode = resultCode;
        this.resultValue = resultValue;
    }

    public int getResultCode() {
        return resultCode;
    }

    public Object getResultValue() {
        return resultValue;
    }

    //----------------------------------------------------------------------------
    public interface EventCode {
        int LoggedOut = 0;
        int LoggedIn = 1;
        int Refresh = 2;
        int ShowProgress = 3;
        int HideProgress = 4;
        int LoadedData = 5;
        int UpdateProfile = 6;
    }
}
