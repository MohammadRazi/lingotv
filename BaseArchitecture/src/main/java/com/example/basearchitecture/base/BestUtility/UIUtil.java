package com.example.basearchitecture.base.BestUtility;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class UIUtil {

    public static int getColor(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }

    public static int getColor(int id) {
        return getColor(UsefulUtility.getInstance(), id);
    }

    public static Drawable getDrawableFromUri(Uri uri) {
        Drawable drawable = null;
        try {
            InputStream inputStream = UsefulUtility.getInstance().getContentResolver().openInputStream(uri);
            drawable = Drawable.createFromStream(inputStream, uri.toString());
        } catch (FileNotFoundException e) {
            return null;
        }

        return drawable;
    }

    public static Drawable getDrawable(int ResID) {
        return ResourcesCompat.getDrawable(UsefulUtility.getInstance().getResources(), ResID, null);
    }

    public static int getThemeColor(Context context, int colorId) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(colorId, typedValue, true);
        return typedValue.data;
    }

    public static Drawable getDrawable(Context context, int drawableId) {
        return ContextCompat.getDrawable(context, drawableId);
    }

    public static Bitmap takeScreenShot(Activity activity) {

        View decorView = activity.getWindow().getDecorView();
        decorView.setDrawingCacheEnabled(true);
        decorView.buildDrawingCache();
        Rect rect = new Rect();
        decorView.getWindowVisibleDisplayFrame(rect);
        int statusBarHeight = rect.top;
        Bitmap cache = decorView.getDrawingCache();
        if (cache == null) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(cache, 0, statusBarHeight, decorView.getMeasuredWidth(), decorView.getMeasuredHeight() - statusBarHeight);
        decorView.destroyDrawingCache();
        return bitmap;
    }

    public static int getScreenWidth(Context context) {
        return getScreenSize(context, null).x;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int getDimensionInPixelResource(int i) {
        return UsefulUtility.getInstance().getResources().getDimensionPixelSize(i);
    }

    public static int getScreenHeight(Context context) {
        return getScreenSize(context, null).y;
    }

    public static Point getScreenSize(Context context, Point outSize) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point ret = outSize == null ? new Point() : outSize;
        Display defaultDisplay = null;

        if (wm != null) {
            defaultDisplay = wm.getDefaultDisplay();
        }

        if (defaultDisplay != null) {
            defaultDisplay.getSize(ret);
        }
        return ret;
    }

    public static int getDpi(Context context) {
        return context.getResources().getDisplayMetrics().densityDpi;
    }

    public static void closeKeyBoard(Activity mActivity) {
        // Check if no view has focus:
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hiddenView(final ViewGroup view) {
       /* AnimUtils.with(Techniques.FadeOut)
                .duration(1000)
                .onEnd(animator -> view.setVisibility(View.GONE))
                .playOn(view);*/
    }

    public static void showView(final ViewGroup view) {
        /*view.setVisibility(View.VISIBLE);
        AnimUtils.with(Techniques.FadeIn)
                .duration(1000)
                .playOn(view);*/
    }

    public static void setTintedCompoundDrawable(TextView textView, int drawableRes, int tintRes) {
        textView.setCompoundDrawablesWithIntrinsicBounds(
                null,  // Left
                null, // Top
                tintDrawable(getDrawable(drawableRes), getColor(tintRes)), // Right
                null); //Bottom
        // if you need any space between the icon and text.
        textView.setCompoundDrawablePadding(12);
    }

    public static Drawable tintDrawable(Drawable drawable, int tint) {
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, tint);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_ATOP);

        return drawable;
    }

    public static int getStatusBarHeight() {
        int result = 0;
        int resourceId = UsefulUtility.getInstance().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = UsefulUtility.getInstance().getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void showSoftKey(Activity activity, EditText editText) {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int DpToPx(Context context, int dpId) {
        return (int) context.getResources().getDimension(dpId);
    }

    public static float dpToPixel(Context c, float dp) {
        float density = c.getResources().getDisplayMetrics().density;
        return dp * density;
    }

    public static int dipToPx(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int pxToDip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
}
