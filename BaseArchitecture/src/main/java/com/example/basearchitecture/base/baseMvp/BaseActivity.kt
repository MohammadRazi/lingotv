package com.example.basearchitecture.base.baseMvp

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import butterknife.ButterKnife
import butterknife.Unbinder
import com.example.basearchitecture.R
import com.example.basearchitecture.base.BestUtility.ActivityUtils
import com.example.basearchitecture.base.BestUtility.AlertDialogUtils
import com.example.basearchitecture.base.BestUtility.SystemUtils
import com.example.basearchitecture.base.EventBusModel
import com.example.basearchitecture.base.component.CustomToast
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

abstract class BaseActivity : AppCompatActivity(), IBaseView {
    protected lateinit var unbinder: Unbinder
    protected lateinit var activity: AppCompatActivity
    protected lateinit var context: Context
    protected lateinit var progressDialog: ProgressDialog

    protected abstract val contentViewId: Int

    protected abstract fun onEvent(event: EventBusModel)

    protected abstract fun initBeforeView()

    protected abstract fun initNavigation()

    protected abstract fun initToolbar()

    protected abstract fun initViews()

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onEventBus(event: EventBusModel) {
        onEvent(event)
    }

    //region Lifecycle Methods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initBeforeView()

        setContentView(contentViewId)

        activity = this
        context = this

        unbinder = ButterKnife.bind(this)

        ActivityUtils.getInstance().hideKeyboard(activity)
       // ActivityUtils.getInstance().setRTL(activity)

        progressDialog = AlertDialogUtils.generateProgressDialog(activity, "در حال ارتباط با سرور...")

        initNavigation()
        initToolbar()
        initViews()

    }

    public override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    public override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    //    @Override
    //    protected void onPause() {
    //        EventBus.getDefault().unregister(this);
    //        super.onPause();
    //    }
    //
    //
    //
    //    @Override
    //    protected void onResume() {
    //        EventBus.getDefault().register(this);
    //        super.onResume();
    //    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgress()
        unbinder.unbind()
    }

    override fun finish() {
        super.finish()
        overridePendingTransitionExit()
    }

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
       overridePendingTransitionEnter()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()

        ActivityUtils.getInstance().hideKeyboard(activity)
       overridePendingTransitionExit()
    }
    //endregion

    //region IMasterActivity
    override fun networkConnectionError() {
        AlertDialogUtils.ShowAlertDialog(activity, "لطفا ارتباط با اینترنت را بررسی کرده و دوباره تلاش کنید.")
    }

    override fun unableToConnect() {
        CustomToast.makeText(activity, "ارتباط با سرور برقرار نشد!", CustomToast.ERROR)
    }

    override fun showMessage(message: String) {
        AlertDialogUtils.ShowAlertDialog(activity, message)
    }

    override fun showError(errorMessage: String) {
        CustomToast.makeText(activity, errorMessage, CustomToast.ERROR)
    }

    override fun showProgress() {
        AlertDialogUtils.setProgressDialogStatus(true, progressDialog)
    }

    override fun hideProgress() {
        AlertDialogUtils.setProgressDialogStatus(false, progressDialog)
    }

    override fun vibrate() {
        SystemUtils.vibrate()
    }

    override fun onBack() {
        onBackPressed()
    }

    override fun setProcess(progress: Int) {
        progressDialog.setMessage("$progress%")
        showProgress()
    }

    //endregion

    //region Transition Methods
    private fun overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left)
    }

    private fun overridePendingTransitionExit() {
      overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right)
    }
    //endregion
}
