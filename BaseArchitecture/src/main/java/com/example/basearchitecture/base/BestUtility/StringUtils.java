package com.example.basearchitecture.base.BestUtility;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.EditText;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class StringUtils {
    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static boolean isEmpty(EditText editText) {
        return editText.getText().toString().isEmpty();
    }

    public int isNullOrEmptyLength(EditText editText) {
        boolean b = editText.getText() == null || editText.getText().toString().trim().length() <= 0;
        if (b)
            return 0;
        else
            return editText.getText().toString().trim().length();
    }

    public int length(EditText editText) {
        return editText.getText().toString().trim().length();
    }

    public SpannableString Underline(String val) {
        SpannableString content = new SpannableString(val);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    public String ArabicToPersian(String inputString) {
        return inputString
                .replace("ي", "ی")
                .replace("و", "و")
                .replace("ك", "ک");
    }

    public String DecodeStringToUTF8(String inputString) {
        //                        res = res.replace("064A", "06CC");

        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(inputString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public String FixString(String inputString) {
        String string = DecodeStringToUTF8(inputString);
        return ArabicToPersian(string);
    }

    public JSONObject FixStringToJson(String inputString) {
        String string = DecodeStringToUTF8(inputString);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(ArabicToPersian(string));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);
    }

    public static String NumSeparator(String num) {
        try {
            if (!isEmpty(num)) {
                double amount = Double.parseDouble(num);
                DecimalFormat formatter = new DecimalFormat("#,###");
                return formatter.format(amount);
            } else {
                return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }

    public String NumSeparator(Double num) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(num);
    }

    public static String NumSeparator(int num) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(num);
    }

    public Double CalculateTax(String num) {
        return Double.parseDouble(num) * 1.09;
    }

//    public String bodyToString(final Request request) {
//        try {
//            Request copy = request.newBuilder().build();
//            Buffer buffer = new Buffer();
//            copy.body().writeTo(buffer);
//            return buffer.readUtf8();
//        } catch (IOException e) {
//            return "";
//        }
//    }
}
