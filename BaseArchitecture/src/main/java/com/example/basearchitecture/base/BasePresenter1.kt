package com.example.user.nlingotv.base

import android.content.Context

import io.reactivex.disposables.CompositeDisposable

class BasePresasdenter<V> {
    protected var view: V? = null
    protected var context: Context?=null
    protected var compositeDisposable: CompositeDisposable? = CompositeDisposable()

    protected fun attachView(view: V, context: Context) {
        this.view = view
        this.context = context
    }

    fun detachView() {
        this.view = null
    }

    fun unsubscribe() {
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
        }
    }
}