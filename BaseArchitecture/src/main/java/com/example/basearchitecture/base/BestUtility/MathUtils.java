package com.example.basearchitecture.base.BestUtility;

import java.math.BigDecimal;

public class MathUtils {
    public static int Round_UP(double value) {
        BigDecimal b = new BigDecimal(value);
        b = b.setScale(0, BigDecimal.ROUND_UP);
        return (int) b.doubleValue();
    }

    public static int Round_UP(String value) {
        BigDecimal b = new BigDecimal(value);
        b = b.setScale(0, BigDecimal.ROUND_UP);
        return (int) b.doubleValue();
    }

    public static int Round_Down(double value) {
        BigDecimal b = new BigDecimal(value);
        b = b.setScale(0, BigDecimal.ROUND_DOWN);
        return (int) (b.doubleValue());
    }

    public static double Round_UP_To_MiddleZero(double value) {

        long iPart;
        double fPart;

        iPart = (long) value;
        fPart = value - iPart;

        BigDecimal b = new BigDecimal(value);
        if ((fPart > 0.5) && (fPart != 0.0)) {
            b = b.setScale(0, BigDecimal.ROUND_UP);
            return b.doubleValue();
        } else if ((fPart < 0.5) && (fPart != 0.0)) {
            b = b.setScale(0, BigDecimal.ROUND_DOWN);
            return b.doubleValue() + 0.5;
        } else {
            return b.doubleValue();
        }
    }
}
