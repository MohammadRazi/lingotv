package com.example.basearchitecture.base.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.example.basearchitecture.R;
import com.example.basearchitecture.base.BestUtility.StringUtils;
import com.example.basearchitecture.base.BestUtility.SystemUtils;

public class NumberStepper extends LinearLayout {
    private static final int MAX_VALUE = 10000;
    private static final int MIN_VALUE = -10000;
    private static final int DEF_VALUE = 0;
    private static final int STEP_VALUE = 1;
    private static final int DISABLE_COLOR = Color.parseColor("#a9a9a9");
    private static final int ENABLE_COLOR = Color.parseColor("#e4215d");

    private View view;
    private AppCompatImageView ibtn_Decrease, ibtn_Increase;
    private EditText txt_Value;

    private int min, max, val, step, enableColor, disableColor;
    private boolean isEditable = false, isVibrate = false, isAccept = true, isValid = true;

    private OnButtonClickListener onButtonClickListener;
    private OnTextChangedListener onTextChangedListener;

    public NumberStepper(Context context, AttributeSet attrs) {
        super(context, attrs);

        disableColor = DISABLE_COLOR;
        enableColor = ENABLE_COLOR;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.NumberStepper, 0, 0);
        try {
            enableColor = ta.getColor(R.styleable.NumberStepper_color, ENABLE_COLOR);
            step = ta.getInteger(R.styleable.NumberStepper_stepValue, STEP_VALUE);
            val = ta.getInteger(R.styleable.NumberStepper_defValue, DEF_VALUE);
            min = ta.getInteger(R.styleable.NumberStepper_minValue, MIN_VALUE);
            max = ta.getInteger(R.styleable.NumberStepper_maxValue, MAX_VALUE);
            isEditable = ta.getBoolean(R.styleable.NumberStepper_editable, false);
            isVibrate = ta.getBoolean(R.styleable.NumberStepper_vibrate, true);
        } finally {
            ta.recycle();
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.number_stepper, this, true);

        txt_Value = view.findViewById(R.id.txt_Value);
        ibtn_Increase = view.findViewById(R.id.ibtn_Increase);
        ibtn_Decrease = view.findViewById(R.id.ibtn_Decrease);

        txt_Value.setTextColor(enableColor);
        txt_Value.setEnabled(isEditable());
        txt_Value.setText(String.valueOf(val));

        ChangeStateViews();

        if (isEditable()) {
            //region isEditable
            txt_Value.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        CalculateValueWithMinAndMax();
                    }
                }
            });

            txt_Value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!StringUtils.isEmpty(s.toString())) {
                        val = Integer.parseInt(s.toString());
                    } else {
                        val = min;
                    }

                    ChangeStateViews();

                    if (onTextChangedListener != null) {
                        onTextChangedListener.onTextChanged(view, val);
                    }
                }
            });

            //endregion
        }

        //region ibtn_Increase.setOnClickListener
        ibtn_Increase.setOnClickListener(v -> {
            if (StringUtils.isEmpty(txt_Value)) {
                txt_Value.setText(String.valueOf(min));
            }

            int number = Integer.parseInt(txt_Value.getText().toString());

            if (isAccept) {
                if (number < max) {
                    if (isVibrate) {
                        SystemUtils.vibrate_short();
                    }

                    val = number + step;
                    txt_Value.setText(String.valueOf(val));
                }
            } else {
                val = number;
            }

            if (onButtonClickListener != null) {
                isAccept = onButtonClickListener.onClickListener(view, val, EventName.Increase);
            }

            ChangeStateViews();

            if (isEditable()) {
                txt_Value.setSelection(txt_Value.getText().length());
            }
        });
        //endregion

        //region ibtn_Decrease.setOnClickListener
        ibtn_Decrease.setOnClickListener(v -> {
            if (StringUtils.isEmpty(txt_Value)) {
                txt_Value.setText(String.valueOf(max));
            }

            int number = Integer.parseInt(txt_Value.getText().toString());

            if (isAccept) {
                if (number > min) {
                    if (isVibrate) {
                        SystemUtils.vibrate_short();
                    }

                    val = number - step;
                    txt_Value.setText(String.valueOf(val));
                }
            } else {
                val = number;
            }

            if (onButtonClickListener != null) {
                isAccept = onButtonClickListener.onClickListener(view, val, EventName.Decrease);
            }

            ChangeStateViews();

            if (isEditable()) {
                txt_Value.setSelection(txt_Value.getText().length());
            }
        });
        //endregion

        //        //region ibtn_Increase.setOnLongClickListener
//        ibtn_Increase.setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    LogApp.w("TAG", "NumberStepper_onTouch_1-> :" + "ACTION_DOWN");
//
//                    ic_timer = new Timer();
//                    ic_timer.scheduleAtFixedRate(new TimerTask() {
//                        @Override
//                        public void run() {
//
//                            DispatchThread.getInstance().postRunnableOnUI(new Runnable() {
//                                @Override
//                                public void run() {
//                                    //region dd
//
//                                    LogApp.w("TAG", "NumberStepper_onTouch_2-> :" + "ACTION_DOWN");
//
//                                    if (StringUtils.getInstance().isEmpty(txt_Value)) {
//                                        txt_Value.setText(String.valueOf(min));
//                                    }
//
//                                    int number = Integer.parseInt(txt_Value.getText().toString());
//
//                                    if (isAccept) {
//                                        if (number < max) {
////                                            if (isVibrate) {
////                                                BaseUtils.SystemUtils.vibrate_short();
////                                            }
//
//                                            val = number + 1;
//                                            txt_Value.setText(String.valueOf(val));
//                                        }
//                                    } else {
//                                        val = number;
//                                    }
//
//                                    ChangeStateViews();
//
//                                    if (isEditable()) {
//                                        txt_Value.setSelection(txt_Value.getText().length());
//                                    }
//                                    //endregion
//                                }
//                            });
//
//                        }
//                    }, 100, 100);
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    ic_timer.cancel();
//                    ic_timer = null;
//                }
//
//                return true;
//            }
//        });
//        //endregion
    }

    private void ChangeStateViews() {
//        val = Integer.parseInt(txt_Value.getText().toString());

        isValid = val <= max && val >= min;

        txt_Value.setEnabled(isEditable());

        ChangeState_Increase(true);
        ChangeState_Decrease(true);

        if (val <= min) {
            ChangeState_Decrease(false);
        }

        if (val >= max) {
            ChangeState_Increase(false);
        }

        if (val == max && val == min) {
            txt_Value.setTextColor(disableColor);
            txt_Value.setEnabled(false);
        } else {
            txt_Value.setTextColor(enableColor);
        }
    }

    private void ChangeState_Increase(boolean b) {
        ibtn_Increase.setEnabled(b);

        if (b) {
            ibtn_Increase.setColorFilter(enableColor, android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            ibtn_Increase.setColorFilter(disableColor, android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void ChangeState_Decrease(boolean b) {
        ibtn_Decrease.setEnabled(b);

        if (b) {
            ibtn_Decrease.setColorFilter(enableColor, android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            ibtn_Decrease.setColorFilter(disableColor, android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void CalculateValueWithMinAndMax() {
        if (!StringUtils.isEmpty(txt_Value)) {
            try {
                int number = Integer.parseInt(txt_Value.getText().toString());

                if (number > max) {
                    txt_Value.setText(String.valueOf(max));
                } else if (number < min) {
                    txt_Value.setText(String.valueOf(min));
                }

                val = Integer.parseInt(txt_Value.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
                val = min;
            }
        } else {
            val = min;
        }

        txt_Value.setText(String.valueOf(val));

        ChangeStateViews();
    }

    //region Properties
    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getValue() {
        return val;
    }

    public void setValue(int value) {
        this.val = value;
        txt_Value.setText(String.valueOf(val));

        ChangeStateViews();
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;

        if (val > min) {
            val = min;
            txt_Value.setText(String.valueOf(val));
        }

        ChangeStateViews();
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;

        if (val > max) {
            val = max;
            txt_Value.setText(String.valueOf(val));
        }

        ChangeStateViews();
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        this.isEditable = editable;
    }

    public boolean isVibrate() {
        return isVibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.isVibrate = vibrate;
    }

    public boolean isValid() {
        return isValid;
    }

//    public void setValid(boolean valid) {
//        isValid = valid;
//    }

    public int getEnableColor() {
        return enableColor;
    }

    public void setEnableColor(int enableColor) {
        this.enableColor = enableColor;
    }

    public int getDisableColor() {
        return disableColor;
    }

    public void setDisableColor(int disableColor) {
        this.disableColor = disableColor;
    }

    //endregion

    //region Listener
    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener) {
        this.onButtonClickListener = onButtonClickListener;
    }

    public void setOnChangeListener(OnTextChangedListener onTextChangedListener) {
        this.onTextChangedListener = onTextChangedListener;
    }

    public interface OnButtonClickListener {
        boolean onClickListener(View view, int val, EventName event);
    }

    public interface OnTextChangedListener {
        boolean onTextChanged(View view, int val);
    }
    //endregion

    public enum EventName {
        Increase(1),
        Decrease(2);

        private final int value;

        EventName(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
