package com.example.basearchitecture.base.component;

import android.content.Context;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import java.util.Arrays;

/**
 * Created by Dev100 on 2018/02/17.
 */

public class CustomAutoCompleteTextView extends androidx.appcompat.widget.AppCompatAutoCompleteTextView {
    public CustomAutoCompleteTextView(Context context) {
        super(context);
    }

    class Validator implements AutoCompleteTextView.Validator {
        String[] array;

        public Validator(String[] array) {
            this.array = array;
        }

        @Override
        public boolean isValid(CharSequence text) {
            Arrays.sort(array);
            return Arrays.binarySearch(array, text.toString()) > 0;
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.v("Test", "Returning fixed text");

            return "";
        }
    }

//    class FocusListener implements View.OnFocusChangeListener {
//        @Override
//        public void onFocusChange(View v, boolean hasFocus) {
//            LogApp.v("Test", "Focus changed");
//            if (v.getId() == R.id.countries_list && !hasFocus) {
//                LogApp.v("Test", "Performing validation");
//                ((AutoCompleteTextView) v).performValidation();
//            }
//        }
//    }
}

