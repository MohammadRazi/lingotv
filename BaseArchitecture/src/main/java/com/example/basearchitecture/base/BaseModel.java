package com.example.basearchitecture.base;

import androidx.annotation.NonNull;
import com.google.gson.Gson;

public class BaseModel {
    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
