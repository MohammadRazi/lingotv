package com.example.basearchitecture.base.baseMvp;

public interface IBaseView {
    void networkConnectionError();

    void unableToConnect();

    void showMessage(String message);

    void showError(String errorMessage);

    void showProgress();

    void hideProgress();

    void setProcess(int progress);

    void vibrate();

    void onBack();
}
