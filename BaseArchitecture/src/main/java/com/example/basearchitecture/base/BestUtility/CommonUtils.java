package com.example.basearchitecture.base.BestUtility;

import java.util.Random;

public class CommonUtils {

    /**
     * generate numbers from min to max (including both)
     */
    public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public static String installedCount(int count) {
        if (count >= 0 && count <= 100)
            return "کمتر از 100";
        else if (count > 100 && count <= 500)
            return "+100";
        else if (count > 500 && count <= 1000)
            return "+500";
        else if (count > 1000 && count <= 5000)
            return "+1000";
        else if (count > 5000 && count <= 10000)
            return "+5000";
        else if (count > 10000 && count <= 20000)
            return "+10000";
        else if (count > 20000 && count <= 50000)
            return "+20000";
        else return "+50000";
    }

    public static String twoDigitString(long number) {
        if (number == 0)
            return "00";

        if (number / 10 == 0)
            return "0" + number;
        return String.valueOf(number);
    }
}
