package com.example.basearchitecture.base.BestUtility;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Vibrator;

import java.util.List;

public class SystemUtils {
    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    LogApp.i("SystemUtils", "isServiceRunning   : True");
                    return true;
                }
            }
        }

        LogApp.i("SystemUtils", "isServiceRunning   : False");
        return false;
    }

    public static boolean isServiceRunning(Class<?> serviceClass) {
        return isServiceRunning(UsefulUtility.getInstance(), serviceClass);
    }

    public static boolean isAppRunning(Context context) {
        final String packageName = context.getPackageName();
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> runningAppProcessesInfoList = activityManager != null ? activityManager.getRunningAppProcesses() : null;

        if (runningAppProcessesInfoList != null) {
            for (final ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcessesInfoList) {
                if (runningAppProcessInfo.processName.equals(packageName)) {
                    LogApp.i("SystemUtils", "isAppRunning     : True");
                    return true;
                }
            }
        }

        LogApp.i("SystemUtils", "isAppRunning     : False");
        return false;
    }

    public static boolean isAppRunning() {
        return isAppRunning(UsefulUtility.getInstance());
    }

    public static boolean isAppOnBackground(Context context) {
        final String packageName = context.getPackageName();
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> runningAppProcessesInfoList = activityManager != null ? activityManager.getRunningAppProcesses() : null;

        if (runningAppProcessesInfoList != null) {
            for (final ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcessesInfoList) {
                if (runningAppProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND && runningAppProcessInfo.processName.equals(packageName)) {
                    LogApp.i("SystemUtils", "isAppOnBackground: True");
                    return true;
                }
            }
        }

        LogApp.i("SystemUtils", "isAppOnBackground: False");
        return false;
    }

    public static boolean isAppOnBackground() {
        return isAppOnBackground(UsefulUtility.getInstance());
    }

    public static boolean isAppOnForeground(Context context) {
        final String packageName = context.getPackageName();
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> runningAppProcessesInfoList = activityManager != null ? activityManager.getRunningAppProcesses() : null;

        if (runningAppProcessesInfoList != null) {
            for (final ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcessesInfoList) {
                if (runningAppProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && runningAppProcessInfo.processName.equals(packageName)) {
                    LogApp.i("SystemUtils", "isAppOnForeground: True");
                    return true;
                }
            }
        }

        LogApp.i("SystemUtils", "isAppOnForeground: False");
        return false;
    }

    public static boolean isAppOnForeground() {
        return isAppOnForeground(UsefulUtility.getInstance());
    }

    //-------------------------------------------------------------------------------
    public static boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) UsefulUtility.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static void vibrate() {
        try {
            Vibrator v = (Vibrator) UsefulUtility.getInstance().getSystemService(Context.VIBRATOR_SERVICE);
            if (v != null) {
                v.vibrate(200);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void vibrate_short() {
        try {
            Vibrator v = (Vibrator) UsefulUtility.getInstance().getSystemService(Context.VIBRATOR_SERVICE);
            if (v != null) {
                v.vibrate(50);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String GetSystemInfo() {
        SystemInfoModel systemInfoModel = new SystemInfoModel();
        systemInfoModel.setOSVersion(System.getProperty("os.version"));
        systemInfoModel.setSDK(String.valueOf(Build.VERSION.SDK_INT));
        systemInfoModel.setModel(Build.MODEL);
        systemInfoModel.setManufacturer(Build.MANUFACTURER);
        systemInfoModel.setBoard(Build.BOARD);
        systemInfoModel.setDisplay(Build.DISPLAY);
        systemInfoModel.setHost(Build.HOST);
        systemInfoModel.setId(Build.ID);
        systemInfoModel.setType(Build.TYPE);
        systemInfoModel.setUser(Build.USER);
        systemInfoModel.setSerial(Build.SERIAL);
        systemInfoModel.setTags(Build.TAGS);
        return systemInfoModel.toString();
    }

    public static String GetClassName(Class c) {
        String fullClassName = c.getName();
        String simpleClassName = fullClassName.substring(fullClassName.lastIndexOf('.') + 1);
        simpleClassName = simpleClassName.substring(0, simpleClassName.length() - 2);

        return simpleClassName;
    }
}
