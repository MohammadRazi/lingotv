package com.example.basearchitecture.base.baseMvp

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import butterknife.Unbinder
import com.example.basearchitecture.base.BestUtility.SystemUtils
import com.example.basearchitecture.base.component.CustomToast


abstract class BaseFragmentDialog : DialogFragment(), IBaseView {



    private var mActivity: BaseActivity? = null
    private var mUnBinder: Unbinder? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity)
            this.mActivity = context
    }

    override fun onDetach() {
        mActivity = null
        super.onDetach()
    }

    override fun networkConnectionError() {
        mActivity?.networkConnectionError()
    }

    override fun unableToConnect() {
        mActivity?.unableToConnect()
    }

    override fun showMessage(message: String?) {
        CustomToast.makeText(context, message, 3)
    }

    override fun showError(errorMessage: String?) {
        if (errorMessage != null) {
            mActivity?.showError(errorMessage)
        }
    }

    override fun showProgress() {
        mActivity?.showProgress()
    }

    override fun hideProgress() {
        mActivity?.hideProgress()
    }

    override fun vibrate() {
        SystemUtils.vibrate()
    }

    override fun onBack() {
        mActivity?.onBack()

    }

    override fun setProcess(progress: Int) {
        mActivity?.setProcess(progress)

    }

    fun setUnBinder(unBinder: Unbinder) {
        mUnBinder = unBinder
    }

    protected abstract fun setUp(view: View)

    /* companion object {
        fun newInstance(VA:DialogFragment): DialogFragment {
            val args = Bundle()
            val fragment = VA
            fragment.setArguments(args)
            return fragment
        }
    }*/




    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        // creating the fullscreen dialog
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp(view)
    }

    fun showDialog(fragmentManager: FragmentManager, tag: String) {
        val transaction = fragmentManager.beginTransaction()
        val prevFragment = fragmentManager.findFragmentByTag(tag)
        if (prevFragment != null) {
            transaction.remove(prevFragment)
        }
        transaction.addToBackStack(null)
        show(transaction, tag)
    }

    override fun onDestroy() {
        mUnBinder?.unbind()
        super.onDestroy()
    }
}