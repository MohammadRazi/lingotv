package com.example.basearchitecture.base.baseMvp

import android.os.Bundle
import com.example.user.nlingotv.base.BasePresenter

abstract class MvpActivity<P : BasePresenter<*>> : BaseActivity() {
    protected var presenter: P? = null

    protected abstract fun createPresenter(): P

    override fun onCreate(savedInstanceState: Bundle?) {
        presenter = createPresenter()
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (presenter != null) {
            presenter!!.unSubscribe()
            presenter!!.deAttachView()
        }
    }
}