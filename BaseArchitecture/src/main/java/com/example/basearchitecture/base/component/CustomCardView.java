package com.example.basearchitecture.base.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import com.example.basearchitecture.R;

public class CustomCardView extends CardView {
    private boolean isDisableDoubleClick = true;
    private int doubleClickDelay = 1000;
    private long lastClickTime = 0;
    //==================================================================================

    public CustomCardView(Context context) {
        super(context);
        initViews(context, null);
    }

    public CustomCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context, attrs);
    }

    public CustomCardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(context, attrs);
    }

    private void initViews(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomCardView, 0, 0);

        try {
            isDisableDoubleClick = typedArray.getBoolean(R.styleable.CustomCardView_disableDoubleClick, true);
        } finally {
            typedArray.recycle();
        }
    }
    //==================================================================================

    @Override
    public boolean performClick() {
        if (isDisableDoubleClick) {
            //region Preventing multiple clicks, using threshold of 1 second
            if (SystemClock.elapsedRealtime() - lastClickTime < doubleClickDelay) {
                return false;
            }

            lastClickTime = SystemClock.elapsedRealtime();
            //endregion
            return super.performClick();
        } else {
            return super.performClick();
        }
    }

    @Override
    public void setOnClickListener(@Nullable View.OnClickListener l) {
        super.setOnClickListener(l);
    }
    //==================================================================================

    //region isDisableDoubleClick | Getter/Setter
    public boolean isDisableDoubleClick() {
        return isDisableDoubleClick;
    }

    public void setDisableDoubleClick(boolean disableDoubleClick) {
        isDisableDoubleClick = disableDoubleClick;
    }
    //endregion

    //region doubleClickDelay | Getter/Setter
    public int getDoubleClickDelay() {
        return doubleClickDelay;
    }

    public void setDoubleClickDelay(int doubleClickDelay) {
        this.doubleClickDelay = doubleClickDelay;
    }

    //endregion
}
