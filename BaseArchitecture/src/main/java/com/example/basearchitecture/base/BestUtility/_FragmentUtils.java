package com.example.basearchitecture.base.BestUtility;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import com.example.basearchitecture.R;

public class _FragmentUtils {
    private static _FragmentUtils ourInstance = new _FragmentUtils();

    private _FragmentUtils() {
    }

    public static _FragmentUtils getInstance() {
        if (ourInstance == null)
            ourInstance = new _FragmentUtils();

        return ourInstance;
    }

    public void addFragment(AppCompatActivity activity, @IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    public void replaceFragment(AppCompatActivity activity, @IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag, @Nullable String backStackStateName) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }

    public void replaceFragment(AppCompatActivity activity, @IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentManager.addOnBackStackChangedListener(() -> {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        });

        fragmentTransaction.replace(containerViewId, fragment, fragmentTag);
//        fragmentTransaction.addToBackStack("h");
        fragmentTransaction.commit();

//        activity.getSupportFragmentManager()
//                .beginTransaction()
////                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
//                .replace(containerViewId, fragment, fragmentTag)
////                .addToBackStack(backStackStateName)
//                .commit();
    }

}