package com.example.basearchitecture.base.BestUtility;

import android.content.Context;

public class UsefulUtility {
    private static Context appContext;

    public static Context getInstance() {
        return appContext;
    }

    public static void init(Context context) {
        appContext = context.getApplicationContext();
    }

//
//    private Builder _builder;
//    private UsefulUtility(Builder builder)
//    {
//        _builder= builder;
//    }
//
//    public void xxx() {
//        Context x = _builder.context;
//    }
//
//    public static Builder setContext(Context context) {
//        return new Builder(context);
//    }

//
//    public static class Builder
//    {
//         Context context;
//
//        private Builder(Context context) {
//            this.context = context;
//        }
//
//        public  UsefulUtility  init()
//        {
//            return new UsefulUtility(this);
//        }
//    }
}
