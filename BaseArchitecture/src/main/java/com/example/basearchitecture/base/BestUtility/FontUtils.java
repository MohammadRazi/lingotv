package com.example.basearchitecture.base.BestUtility;

import android.graphics.Typeface;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;
import androidx.collection.SimpleArrayMap;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.*;
import android.widget.TextView;

public class FontUtils {
    private static final String PATH = "fonts/%s.ttf";
    private static final SimpleArrayMap<String, Typeface> cache = new SimpleArrayMap<>();
    private static Typeface typeface;
    private static FontUtils.Font font = new FontUtils().new Font();

    //----------------------------------------------------------------------------------------
    private static Typeface GetFontFromCache(String fontName) {
        synchronized (cache) {
            String path = String.format(PATH, fontName);
            if (!cache.containsKey(fontName)) {
                Typeface t = Typeface.createFromAsset(UsefulUtility.getInstance().getAssets(), path);
                cache.put(fontName, t);
                return t;
            }
            return cache.get(fontName);
        }
    }

    public static Typeface getTypeface() {
        return font.getTypeface();
    }

    public static Font Default() {
        return F_Vazir();
    }

    //----------------------------------------------------------------------------------------
    public static Font F_Consolas() {
        typeface = GetFontFromCache("Consolas");
        return font;
    }

    public static Font F_Vazir() {
        typeface = GetFontFromCache("Vazir");
        return font;
    }

    public static Font F_IranYekan() {
        typeface = GetFontFromCache("IranYekan");
        return font;
    }

    //----------------------------------------------------------------------------------------
    public class Font {
        public Typeface getTypeface() {
            if (typeface == null) {
                Default();
            }

            return typeface;
        }

        public void setTypeface(Typeface typeface) {
            FontUtils.typeface = typeface;
        }

        //----------------------------------------------------------------------------------------
        public void setFont(TextInputLayout view) {
            view.setTypeface(typeface);

            View child = view.getChildAt(0);
            setFont(child);
        }

        public void setFont(View view) {
            try {
                if (view instanceof ViewGroup && !(view instanceof TextInputLayout)) {
                    ViewGroup vg = (ViewGroup) view;
                    for (int i = 0; i < vg.getChildCount(); i++) {
                        View child = vg.getChildAt(i);
                        setFont(child);
                    }
                } else if (view instanceof TextView) {
                    ((TextView) view).setTypeface(typeface);
                } else if (view instanceof TextInputLayout) {
                    ((TextInputLayout) view).setTypeface(typeface);

                    ViewGroup vg = (ViewGroup) view;
                    View child = vg.getChildAt(0);
                    setFont(child);
                }
            } catch (Exception ignored) {
            }
        }

        public void setFont(TextView view) {
            view.setTypeface(typeface);
        }

        public void setMenuFont(NavigationView nav) {
            Menu m = nav.getMenu();
            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);

                //for applying a font to subMenu ...
                SubMenu subMenu = mi.getSubMenu();
                if (subMenu != null && subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        MenuItem subMenuItem = subMenu.getItem(j);
                        applyFontToMenuItem(subMenuItem);
                    }
                }

                //the method we have create in activity
                applyFontToMenuItem(mi);
            }
        }

        public void applyFontToMenuItem(MenuItem mi) {
            SpannableString mNewTitle = new SpannableString(mi.getTitle());
            mNewTitle.setSpan(new CustomTypefaceSpan("", typeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mi.setTitle(mNewTitle);
        }

        public SpannableStringBuilder applyFontToMenuItem(String string) {
            SpannableStringBuilder SS = new SpannableStringBuilder(string);
            SS.setSpan(new CustomTypefaceSpan("", typeface), 0, string.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

            return SS;
        }
    }
}
