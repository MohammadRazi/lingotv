package com.example.basearchitecture.base.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.widget.Toolbar;
import android.util.AttributeSet;
import com.example.basearchitecture.R;

public class CustomCollapsingToolbarLayout extends CollapsingToolbarLayout {

    private final int toolbarId;
    @Nullable
    private Toolbar toolbar;

    public CustomCollapsingToolbarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTitleEnabled(false);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CollapsingToolbarLayout, 0, R.style.Widget_Design_CollapsingToolbar);
        toolbarId = a.getResourceId(com.google.android.material.R.styleable.CollapsingToolbarLayout_toolbarId, -1);
        a.recycle();
    }

    @Override public void setScrimsShown(boolean shown, boolean animate) {
        super.setScrimsShown(shown, animate);

        findToolbar();
        if (toolbar != null) {
            toolbar.setTitleTextColor(shown ? Color.WHITE : Color.TRANSPARENT);
        }
    }

    private void findToolbar() {
        if (toolbar == null) {
            toolbar =  findViewById(toolbarId);
        }
    }
}
