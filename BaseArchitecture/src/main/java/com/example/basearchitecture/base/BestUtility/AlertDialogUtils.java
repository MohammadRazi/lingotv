package com.example.basearchitecture.base.BestUtility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.basearchitecture.R;

public class AlertDialogUtils {
    public static ProgressDialog generateProgressDialog1(Activity activity, String message, Drawable drawable, boolean cancelable) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(FontUtils.F_Vazir().applyFontToMenuItem(message));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(drawable);
        progressDialog.setCancelable(cancelable);

        return progressDialog;
    }

    public static ProgressDialog generateProgressDialog(Activity activity, String message) {
        ProgressDialog progressDialog = new ProgressDialog(activity);

        progressDialog.setMessage(FontUtils.Default().applyFontToMenuItem(message));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.drawable_progress, null));
        progressDialog.setCancelable(true);

        return progressDialog;
    }

    public static void setProgressDialogStatus(boolean showStatus, ProgressDialog progressDialog) {
        progressDialog.setCancelable(false);
        if (showStatus) {
            progressDialog.show();
        } else if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void ShowAlertDialog(final Activity activity, String title, CharSequence message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialog));

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setNegativeButton(android.R.string.ok, (dialog, id) -> dialog.cancel());

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(arg0 -> {
//                dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setTextColor(Utility.getInstance().getColor(R.color.colorAccent));
//            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(BaseUtils.GraphicUtils.getColor(R.color.m_dialog_Text));

            Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            Button btn_Neutral = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);

            btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
            btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));
            btn_Neutral.setTextColor(UIUtil.getColor(R.color.m_dialog_button_neutral));

            FontUtils.Default().setFont(textView);
            FontUtils.Default().setFont(btn_Positive);
            FontUtils.Default().setFont(btn_Negative);
            FontUtils.Default().setFont(btn_Neutral);

        });

//        dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_bg);
        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;

        dialog.show();
    }

    public static void ShowAlertDialog(Activity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialogCustom));
        builder.setMessage(message);
        builder.setNegativeButton("بسیار خب", (dialog, which) -> dialog.cancel());

        builder.setCancelable(true);

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(arg0 -> {
            Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);

            btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
            btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));

            FontUtils.Default().setFont(btn_Positive);
            FontUtils.Default().setFont(btn_Negative);

            TextView textView = dialog.findViewById(android.R.id.message);
            FontUtils.Default().setFont(textView);

        });

//        dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_bg);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;


        dialog.show();
    }

    public static void ShowAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper());
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(true);

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(UIUtil.getColor(R.color.m_dialog_Text));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(UIUtil.getColor(R.color.CadetBlue));
            }
        });

//        dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_bg);

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;

        dialog.show();
    }

    public static void ShowAlertDialog(Activity activity, String phoneNumber, final IDialog iDialog) {
        String divStart = "<div style='text-align: justify;text-justify: inter-word;direction:rtl;'>";
        String divEnd = "<div style='text-align: justify;text-justify: inter-word;direction:rtl;'>";

        String msg = divStart + "ما برای شما یک پیامک حاوی کد فعالسازی به شماره <b>" + phoneNumber + "</b>" + " ارسال میکنیم." + "<br/><br/>در صورتیکه از صحیح بودن شماره تلفن اطمینان دارید مراحل ثبت نام را تکمیل نمایید." + divEnd;
        Spanned msgHtml;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            msgHtml = Html.fromHtml(msg, Html.FROM_HTML_OPTION_USE_CSS_COLORS);
        } else {
            msgHtml = Html.fromHtml(msg);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialog));
        builder.setMessage(msgHtml);
        builder.setPositiveButton("ادامه", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                iDialog.onPositiveClick();
            }
        });

        builder.setNegativeButton("لغو", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                iDialog.onNegativeClick();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);

                btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
                btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));

                FontUtils.Default().setFont(btn_Positive);
                FontUtils.Default().setFont(btn_Negative);
            }
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;
        dialog.show();
    }

    public static void ShowPromptDialog(Activity activity, String msg, String positiveButtonText, String negativeButtonText, String neutralButtonText, boolean isCancelable, final IDialog iDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialog));
        builder.setMessage(msg);
        if (positiveButtonText != null) {
            builder.setPositiveButton(positiveButtonText, (dialog, which) -> iDialog.onPositiveClick());
        }

        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, (dialog, which) -> iDialog.onNegativeClick());
        }

        if (neutralButtonText != null) {
            builder.setNeutralButton(neutralButtonText, (dialog, which) -> {
//                iDialog.onNegativeClick();
            });
        }

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(arg0 -> {
            Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            Button btn_Neutral = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);

            btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
            btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));
            btn_Neutral.setTextColor(UIUtil.getColor(R.color.m_dialog_button_neutral));

            FontUtils.Default().setFont(textView);
            FontUtils.Default().setFont(btn_Positive);
            FontUtils.Default().setFont(btn_Negative);
            FontUtils.Default().setFont(btn_Neutral);
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;
        dialog.setCancelable(isCancelable);
        dialog.show();
    }

    public static void ShowPromptDialog(Activity activity, String title, String msg, String positiveButtonText, String negativeButtonText, String neutralButtonText, boolean isCancelable, final IDialog iDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialog));
        builder.setTitle(title);
        builder.setMessage(msg);
        if (positiveButtonText != null) {
            builder.setPositiveButton(positiveButtonText, (dialog, which) -> iDialog.onPositiveClick());
        }

        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, (dialog, which) -> iDialog.onNegativeClick());
        }

        if (neutralButtonText != null) {
            builder.setNeutralButton(neutralButtonText, (dialog, which) -> {
//                iDialog.onNegativeClick();
            });
        }

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(arg0 -> {
            Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            Button btn_Neutral = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
            TextView textView = dialog.findViewById(android.R.id.message);

            btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
            btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));
            btn_Neutral.setTextColor(UIUtil.getColor(R.color.m_dialog_button_neutral));

            FontUtils.Default().setFont(textView);
            FontUtils.Default().setFont(btn_Positive);
            FontUtils.Default().setFont(btn_Negative);
            FontUtils.Default().setFont(btn_Neutral);
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;
        dialog.setCancelable(isCancelable);
        dialog.show();
    }

//    public static void ShowPromptDialog(Activity activity, String msg, String positiveButtonText, String negativeButtonText, String neutralButtonText, final IDialog iDialog) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialog));
//        builder.setMessage(msg);
//        if (positiveButtonText != null) {
//            builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    iDialog.onPositiveClick();
//                }
//            });
//        }
//
//        if (negativeButtonText != null) {
//            builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    iDialog.onNegativeClick();
//                }
//            });
//        }
//
//        if (neutralButtonText != null) {
//            builder.setNeutralButton(neutralButtonText, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
////                iDialog.onNegativeClick();
//                }
//            });
//        }
//
//        final AlertDialog dialog = builder.create();
//        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface arg0) {
//                Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
//                Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
//                Button btn_Neutral = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
//                TextView textView = dialog.findViewById(android.R.id.message);
//
//                btn_Positive.setTextColor(BaseUtils.UIUtil.getColor(R.color.m_dialog_button_positive));
//                btn_Negative.setTextColor(BaseUtils.UIUtil.getColor(R.color.m_dialog_button_negative));
//                btn_Neutral.setTextColor(BaseUtils.UIUtil.getColor(R.color.m_dialog_button_neutral));
//
//                FontUtils.setFont(textView);
//                FontUtils.setFont(btn_Positive);
//                FontUtils.setFont(btn_Negative);
//                FontUtils.setFont(btn_Neutral);
//            }
//        });
//
//        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;
//        dialog.show();
//    }

    public static void ShowPromptDialog(Context context, String msg, String positiveButtonText, String negativeButtonText, String neutralButtonText, final IDialog iDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialog));
        builder.setMessage(msg);
        if (positiveButtonText != null) {
            builder.setPositiveButton(positiveButtonText, (dialog, which) -> iDialog.onPositiveClick());
        }

        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, (dialog, which) -> iDialog.onNegativeClick());
        }

        if (neutralButtonText != null) {
            builder.setNeutralButton(neutralButtonText, (dialog, which) -> {
//                iDialog.onNegativeClick();
            });
        }

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(arg0 -> {
            Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            Button btn_Neutral = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
            TextView textView = dialog.findViewById(android.R.id.message);

            btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
            btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));
            btn_Neutral.setTextColor(UIUtil.getColor(R.color.m_dialog_button_neutral));

            FontUtils.Default().setFont(textView);
            FontUtils.Default().setFont(btn_Positive);
            FontUtils.Default().setFont(btn_Negative);
            FontUtils.Default().setFont(btn_Neutral);
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;
        dialog.show();
    }

    public static void ShowPromptDialog(Activity activity, String msg, String positiveButtonText, String negativeButtonText, final IDialog iDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialog));
        builder.setMessage(msg);
        if (positiveButtonText != null) {
            builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    iDialog.onPositiveClick();
                }
            });
        }

        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    iDialog.onNegativeClick();
                }
            });
        }

//        if (negativeButtonText == null && positiveButtonText == null) {
        builder.setNeutralButton("لغو", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                iDialog.onNegativeClick();
            }
        });
//        }

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                Button btn_Positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                Button btn_Negative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                Button btn_Neutral = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
                TextView textView = (TextView) dialog.findViewById(android.R.id.message);

                btn_Positive.setTextColor(UIUtil.getColor(R.color.m_dialog_button_positive));
                btn_Negative.setTextColor(UIUtil.getColor(R.color.m_dialog_button_negative));
                btn_Neutral.setTextColor(UIUtil.getColor(R.color.m_dialog_button_neutral));

                FontUtils.Default().setFont(textView);
                FontUtils.Default().setFont(btn_Positive);
                FontUtils.Default().setFont(btn_Negative);
                FontUtils.Default().setFont(btn_Neutral);
            }
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog;
        dialog.show();
    }

    public static void Show_Toast(String message) {
        Toast.makeText(UsefulUtility.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    public static void Show_Toast(Context context, View view, String error) {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // inflate the layout over view
            View layout = inflater.inflate(R.layout.lay_toast, (ViewGroup) view.findViewById(R.id.toast_root));

            // Get TextView id and set error
            TextView text = layout.findViewById(R.id.toast_error_label);
            text.setText(error);

            FontUtils.Default().setFont(text);

            Toast toast = new Toast(context);// Get Toast Context
            toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
            // Toast
            // gravity
            // and
            // Fill
            // Horizoontal

            toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
            toast.setView(layout); // Set Custom View over toast

            toast.show();// Finally show toast
        } catch (Exception e) {
            Show_Toast(error);
        }
    }

    public static void Show_ToastAlert(Activity activity, String error) {
        try {
            View view = activity.getCurrentFocus();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // inflate the layout over view
            View layout = inflater.inflate(R.layout.lay_toast, (ViewGroup) view.findViewById(R.id.toast_root));

            // Get TextView id and set error
            TextView text = layout.findViewById(R.id.toast_error_label);
            text.setText(error);
            FontUtils.Default().setFont(text);

            Toast toast = new Toast(activity);// Get Toast Context
            toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
            // Toast
            // gravity
            // and
            // Fill
            // Horizoontal

            toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
            toast.setView(layout); // Set Custom View over toast

            toast.show();// Finally show toast
        } catch (Exception e) {
            Show_Toast(error);
        }
    }

    public static void Show_ToastAlert(Activity activity, String error, boolean isVibrate) {
        try {
            Show_ToastAlert(activity, error);

            if (isVibrate) {
                SystemUtils.vibrate();
            }
        } catch (Exception e) {
            Show_Toast(error);
        }
    }

    public static void Show_ToastAlert(Activity activity, View view, String error, boolean isVibrate) {
        try {
            if (view != null) {
                Animation shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                view.startAnimation(shakeAnimation);

                Show_ToastAlert(activity, error, isVibrate);
            }
        } catch (Exception e) {
            Show_Toast(error);
        }
    }

//    public static void showSnackBar(Activity activity, View view, String message) {
//        Snackbar snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG);
//        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
//
//        TextView textView = layout.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setVisibility(View.INVISIBLE);
//
//        View snackView = activity.getLayoutInflater().inflate(R.layout.lay_snackbar, null);
//
//        ImageView imageView = snackView.findViewById(R.id.snackbar_error_image);
//        imageView.setImageDrawable(BaseUtils.UIUtil.getDrawable(R.drawable.ic_vector_error));
//
//        TextView textViewTop = snackView.findViewById(R.id.snackbar_error_label);
//        textViewTop.setText(message);
////        textViewTop.setTextColor(Color.WHITE);
//
//        layout.addView(snackView, 0);
//
////        snackbar.setActionTextColor(Color.RED);
//
//        snackbar.show();
//
//
////        Snackbar snackbar = Snackbar
////                .make(coordinatorLayout, "ارتباط با سرور برقرار نشد!", Snackbar.LENGTH_LONG)
////                .setAction("تلاش مجدد", new View.OnClickListener() {
////                    @Override
////                    public void onClick(View view) {
////                    }
////                });
////
////        snackbar.setActionTextColor(Color.RED);
////
////        View sbView = snackbar.getView();
////        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
////        textView.setTextColor(Color.YELLOW);
////        textView.setTypeface(FontUtils.getInstance().Default());
////
////        snackbar.setActionTextColor(Color.RED);
////        snackbar.show();
//    }

    public static void ShowAlert(Activity activity, String message) {
        ShowAlertDialog(activity, message);
    }

    public interface IDialog {
        void onPositiveClick();

        void onNegativeClick();
    }
}