package com.example.basearchitecture.base.BestUtility;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;
import com.example.basearchitecture.R;
import com.google.gson.Gson;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.WEEK_OF_YEAR;

public class DateTimeUtils {
    public static final String DATA_FORMAT_PATTEN_YYYY_MMMM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATA_FORMAT_PATTEN_YYYY_MMMM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public static final String DATA_FORMAT_PATTEN_YYYY_MMMM_DD = "yyyy-MM-dd";
    public static final String DATA_FORMAT_PATTEN_HH_MM = "HH:mm";

    public static long dateToTimeStamp(String data, String dataFormatPatten) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dataFormatPatten);
        Date date = null;
        try {
            date = simpleDateFormat.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        return date.getTime();
    }

    //convert unix epoch timestamp (seconds) to milliseconds
    public static String timeStampToDateString(long time, String dataFormatPatten) {
        @SuppressLint("SimpleDateFormat")

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        time = time * 1000L;
        cal.setTimeInMillis(time);
        return DateFormat.format(dataFormatPatten, cal).toString();
    }

    //convert milliseconds
    public static String timeStampSecondsToDateString(long time, String dataFormatPatten) {

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format(dataFormatPatten, cal).toString();
    }

    public static String timeStampToDate(long time, String dataFormatPatten) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dataFormatPatten);
        Date date = new Date(time);
        return simpleDateFormat.format(date);
    }

    public static Date dateStringToDate(String dateString, String dataFormatPatten) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dataFormatPatten);
        Date date = new Date();
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        return date;
    }

    public static Calendar dateStringToCalendar(String dateString, String dataFormatPatten) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dataFormatPatten, Locale.ENGLISH);
        try {
            cal.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static String currentJalaliDateTime() {
        Calendar c = Calendar.getInstance();
        return JalaliCalendar.getStrJalaliDate(c.getTime(), true);
    }

    public static Date currentMiladiDateTime() {
        Calendar c = Calendar.getInstance();
        return c.getTime();
    }

    public static String getTimeFromUTC(String utc) {
        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
//        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(utc);
            SimpleDateFormat time = new SimpleDateFormat("HH:mm", Locale.US);

//            LogApp.w("TAG", "DateTimeUtils_getTimeFromUTC_51-> :" + utc + " => " + time.format(date));

            return time.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "--:--";
    }

    public static Date getDateFromUTC(String utc) {
        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

        try {
            return utcFormat.parse(utc);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getDifferenceBetweenTwoDate(Date startDate, Date endDate) {
        long diffInMillis = Math.abs(startDate.getTime() - endDate.getTime());

        long sec = TimeUnit.MILLISECONDS.toSeconds(diffInMillis);

        long hh = TimeUnit.SECONDS.toHours(sec);
        sec -= TimeUnit.HOURS.toSeconds(hh);

        long mm = TimeUnit.SECONDS.toMinutes(sec);
//        sec -= TimeUnit.MINUTES.toSeconds(mm);

//        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hh, mm, sec);


        return String.format(Locale.US, "%d ساعت و %d دقیقه", hh, mm);
    }

    public static long getTimestampFromString(String _date) {
        if (_date == null || _date.isEmpty())
            return 0;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = formatter.parse(_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.getTime().getTime();
    }

    public static long getTimestampFromString(int _year, int _month, int day) {
        String strDate = _year + "/" + _month + "/" + day;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.getTime().getTime();
    }

    public static PersianCalendar getPersianCalendarFromString(String _date) {
        if (_date == null || _date.isEmpty())
            return null;

        DateModel dateModel = splitDate(_date);

        PersianCalendar persianCalendar = new PersianCalendar();
        persianCalendar.setPersianDate(dateModel.getYear(), dateModel.getMonth(), dateModel.getDay());

        return persianCalendar;
    }

    public static String getDate(int _year, int _month, int day) {
        String strDate = _year + "/" + _month + "/" + day;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.setTime(date);

        return DateFormat.format("yyyy/MM/dd", calendar).toString();
    }

    public static String getDate(long time, String dateFormat) {
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(time);
        return DateFormat.format(dateFormat, calendar).toString();
    }

    public static String getDate(long time) {
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(time);
        return DateFormat.format("yyyy/MM/dd", calendar).toString();
    }

    public static long getDate() {
        return System.currentTimeMillis();
    }

    public static String getCurrentDate(String dateFormat) {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat(dateFormat, Locale.US);
        return df.format(c.getTime());
    }

    public static String getDateEasily(Long date, String type) {
        String finalOutput = "";
        switch (type) {
            case "chat": {
                finalOutput = getDate(date, "HH:mm");
            }
            break;
            case "chatDateOrder": {
                finalOutput = getDate(date, "dd/MM/yyyy");
            }
            break;
            case "MessageListAdapter": {
                finalOutput = getDate(date, "hh:mm a dd/MM/yyyy");
            }
            break;
            case "RecentCursorAdapter": {
                finalOutput = getDate(date, "hh:mm a dd/MM/yyyy");
            }
            break;

        }
        return finalOutput;
    }

//    public static String getDateTimePersian(long date) {
//        String stringDate = "";
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(date * 1000L);
//        stringDate = gregorian_to_jalali(stringDate);
//        stringDate = stringDate + " " + DateFormat.format("hh:mm:ss", calendar).toString();
//        return stringDate;
//    }

    public static String getDateTimePersian(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date * 1000L);

        PersianCalendar persianCalendar = new PersianCalendar();
        persianCalendar.setTimeInMillis(calendar.getTimeInMillis());

        return persianCalendar.getPersianShortDate() + " " + DateFormat.format("hh:mm:ss", calendar).toString();
    }

    public static String getDatePersian(long date) {
        String stringDate = "";

        Calendar msgCalendar = Calendar.getInstance();
        Calendar todayCalendar = Calendar.getInstance();
        Calendar yesterdayCalendar = Calendar.getInstance();

        yesterdayCalendar.add(Calendar.DATE, -1);
        msgCalendar.setTimeInMillis(date);

        //region Older Date
        stringDate = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).format(msgCalendar.getTime());

        boolean isPersian = Locale.getDefault().getLanguage().equals("fa");

        if (isPersian) {
            stringDate = gregorian_to_jalali(stringDate);
        }
        //endregion

        return stringDate;
    }

    public static String getDateSummaryForRecentList(long date) {
        String stringDate = "";

        Calendar msgCalendar = Calendar.getInstance();
        Calendar todayCalendar = Calendar.getInstance();
        Calendar yesterdayCalendar = Calendar.getInstance();

        yesterdayCalendar.add(Calendar.DATE, -1);
        msgCalendar.setTimeInMillis(date);

        //region Today
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        if (sdf.format(msgCalendar.getTime()).equals(sdf.format(todayCalendar.getTime()))) {
            stringDate = UsefulUtility.getInstance().getString(R.string.today);
        }
        //endregion

        //region Yesterday
        else if (sdf.format(msgCalendar.getTime()).equals(sdf.format(yesterdayCalendar.getTime()))) {
            stringDate = UsefulUtility.getInstance().getString(R.string.yesterday);
        }
        //endregion

        //region This Week
        else if (msgCalendar.get(WEEK_OF_YEAR) == todayCalendar.get(WEEK_OF_YEAR)) {
            stringDate = msgCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
        }
        //endregion

        //region Older Date
        else {
            stringDate = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).format(msgCalendar.getTime());

            boolean isPersian = Locale.getDefault().getLanguage().equals("fa");

            if (isPersian) {
                stringDate = gregorian_to_jalali(stringDate);
            }
        }
        //endregion

        return stringDate;
    }

    public static String getDateSummaryForMessage(long date) {
        String stringDate = "";
        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names

        Calendar msgCalendar = Calendar.getInstance();
        Calendar todayCalendar = Calendar.getInstance();
        Calendar yesterdayCalendar = Calendar.getInstance();

        yesterdayCalendar.add(Calendar.DATE, -1);
        msgCalendar.setTimeInMillis(date);

        //region Today
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        if (sdf.format(msgCalendar.getTime()).equals(sdf.format(todayCalendar.getTime()))) {
            stringDate = new SimpleDateFormat("hh:mm").format(msgCalendar.getTime());
        }
        //endregion

        //region Yesterday
        else if (sdf.format(msgCalendar.getTime()).equals(sdf.format(yesterdayCalendar.getTime()))) {
            stringDate = UsefulUtility.getInstance().getString(R.string.yesterday);
        }
        //endregion

        //region This Week
        else if (msgCalendar.get(WEEK_OF_YEAR) == todayCalendar.get(WEEK_OF_YEAR)) {
            stringDate = weekdays[msgCalendar.get(DAY_OF_WEEK)];
        }
        //endregion

        //region Older Date
        else {
            stringDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(msgCalendar.getTime());

            boolean isPersian = Locale.getDefault().getLanguage().equals("fa");

            if (isPersian) {
                stringDate = gregorian_to_jalali(stringDate);
            }
        }
        //endregion

        return stringDate;
    }

    public static String getDateSummaryForCallLog(long date) {
        String stringDate;
        String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names

        Calendar msgCalendar = Calendar.getInstance();
        Calendar todayCalendar = Calendar.getInstance();
        Calendar yesterdayCalendar = Calendar.getInstance();

        yesterdayCalendar.add(Calendar.DATE, -1);
        msgCalendar.setTimeInMillis(date);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);

        //region Today
        if (sdf.format(msgCalendar.getTime()).equals(sdf.format(todayCalendar.getTime()))) {
            stringDate = UsefulUtility.getInstance().getString(R.string.today) + " " + new SimpleDateFormat("HH:mm").format(msgCalendar.getTime());
        }
        //endregion
        //region Yesterday
        else if (sdf.format(msgCalendar.getTime()).equals(sdf.format(yesterdayCalendar.getTime()))) {
            stringDate = UsefulUtility.getInstance().getString(R.string.yesterday) + " " + new SimpleDateFormat("HH:mm").format(msgCalendar.getTime());
        }
        //endregion
        //region This Week
        else if (msgCalendar.get(WEEK_OF_YEAR) == todayCalendar.get(WEEK_OF_YEAR)) {
            stringDate = weekdays[msgCalendar.get(DAY_OF_WEEK)] + " " + new SimpleDateFormat("HH:mm").format(msgCalendar.getTime());
        }
        //endregion
        //region Older Date
        else {
            stringDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(msgCalendar.getTime()) + " " + new SimpleDateFormat("HH:mm").format(msgCalendar.getTime());

            boolean isPersian = Locale.getDefault().getLanguage().equals("fa");

            if (isPersian) {
                stringDate = gregorian_to_jalali(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(msgCalendar.getTime())) + " " + new SimpleDateFormat("HH:mm").format(msgCalendar.getTime());
            }
        }
        //endregion

        return stringDate;
    }

    public static String getLastSeenFromDate(long date) {
        String stringDate = "";
        if (date > 0) {
            String[] weekdays = new DateFormatSymbols().getWeekdays(); // Get day names

            Calendar msgCalendar = Calendar.getInstance();
            Calendar todayCalendar = Calendar.getInstance();
            Calendar yesterdayCalendar = Calendar.getInstance();

            yesterdayCalendar.add(Calendar.DATE, -1);
            msgCalendar.setTimeInMillis(date);

            //region Today
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
            if (sdf.format(msgCalendar.getTime()).equals(sdf.format(todayCalendar.getTime()))) {
                stringDate = new SimpleDateFormat("hh:mm").format(msgCalendar.getTime());
            }
            //endregion

            //region Yesterday
            else if (sdf.format(msgCalendar.getTime()).equals(sdf.format(yesterdayCalendar.getTime()))) {
                stringDate = UsefulUtility.getInstance().getString(R.string.yesterday_at) + " " + new SimpleDateFormat("hh:mm").format(msgCalendar.getTime());
            }
            //endregion

            //region This Year
            else if (msgCalendar.get(WEEK_OF_YEAR) == todayCalendar.get(WEEK_OF_YEAR)) {
                stringDate = weekdays[msgCalendar.get(DAY_OF_WEEK)] + " " + UsefulUtility.getInstance().getString(R.string.at) + " " + new SimpleDateFormat("hh:mm").format(msgCalendar.getTime());
            }
            //endregion

            else if (msgCalendar.get(Calendar.YEAR) == todayCalendar.get(Calendar.YEAR)) {
                stringDate = new SimpleDateFormat("dd MMMM", Locale.ENGLISH).format(msgCalendar.getTime()) + " " + UsefulUtility.getInstance().getString(R.string.at) + " " + new SimpleDateFormat("hh:mm").format(msgCalendar.getTime());
            }
            //region Older Date
            else {
                stringDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(msgCalendar.getTime()) + " " + UsefulUtility.getInstance().getString(R.string.at) + " " + new SimpleDateFormat("hh:mm").format(msgCalendar.getTime());
            }
            //endregion
        }
        return stringDate;
    }

    private static String gregorian_to_jalali(String date) {
        LogApp.d("DateTimeUtils", "gregorian_to_jalali:" + date);

        double sal, mah, roz;
        int tmah[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int tmahsh[] = {31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};
        double troz = 0, tsal, mahsh = 0, rozsh, trozsh;
        int j = 0;
        sal = Double.parseDouble(date.split("/")[2]);
        mah = Double.parseDouble(date.split("/")[1]);
        roz = Double.parseDouble(date.split("/")[0]);

        Boolean x = false;//baraye tashkhise kabise bodan
        if (sal % 4 == 0) {
            x = true;
            if (sal % 100 == 0) {
                x = sal % 400 == 0;
            }
        }
        if (x == true)
            tmah[1] = 29;


        //tabdil tarikh be tedad rozhaye separi shode az mabda
        for (int i = 0; i < mah - 1; i++)
            troz += tmah[i];// hamoon zigma ast
        troz += roz;
        sal--;
        sal *= 365;
        troz += sal;
        troz -= 226744;
        tsal = Math.floor(troz / 365);
        trozsh = troz - (tsal * 365);

        do {
            if (trozsh - mahsh < 30)
                break;
            mahsh += tmahsh[j++];
        } while (trozsh - mahsh > 30);
        rozsh = trozsh - mahsh;
        return (String.format("%d/%d/%d", (long) (tsal + 1), (long) (j + 1), (long) rozsh));
    }

    public static long getDiffClientDateWithServerDate(long serverTime) {
        long diffTime = 0;
        long clientTime = new Date().getTime();

        if (serverTime > clientTime) {
            diffTime = serverTime - clientTime;
        } else {
            diffTime = clientTime - serverTime;
        }

        LogApp.w("CheckDateTime", String.format("ServerDateTime= %s\nClientDateTime= %s\nDiffDateTime= %s",
                getDate(serverTime, "yyyy/MM/dd hh:mm:ss"),
                getDate(clientTime, "yyyy/MM/dd hh:mm:ss"),
                TimeUnit.MILLISECONDS.toMinutes(diffTime)));

        return TimeUnit.MILLISECONDS.toMinutes(diffTime);
    }

    public static boolean CheckAccurateDate(long serverTime) {
        return getDiffClientDateWithServerDate(serverTime) < 1;
    }

    public static String getTime_HHMMSS_FromMilliSecond(long milliSecond) {
        long sec = TimeUnit.MILLISECONDS.toSeconds(milliSecond);

        long hh = TimeUnit.SECONDS.toHours(sec);
        sec -= TimeUnit.HOURS.toSeconds(hh);

        long mm = TimeUnit.SECONDS.toMinutes(sec);
        sec -= TimeUnit.MINUTES.toSeconds(mm);

        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hh, mm, sec);
    }

    public static String getTime_MMSS_FromMilliSecond(long milliSecond) {
        long sec = TimeUnit.MILLISECONDS.toSeconds(milliSecond);

        long mm = TimeUnit.SECONDS.toMinutes(sec);
        sec -= TimeUnit.MINUTES.toSeconds(mm);

        return String.format(Locale.getDefault(), "%02d:%02d", mm, sec);
    }

    private static String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public static int getAge(long birthdayTimestamp) {
        long ageInMillis = new Date().getTime() - birthdayTimestamp;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(ageInMillis);
        return cal.get(Calendar.YEAR) - 1970;
    }

    public static DateModel splitDate(String date) {
        int d = Integer.parseInt(String.valueOf(date.split("/")[2]));
        int m = Integer.parseInt(String.valueOf(date.split("/")[1]));
        int y = Integer.parseInt(String.valueOf(date.split("/")[0]));

        LogApp.w("d= " + d + " ,m= " + m + " ,y= " + y);
        return new DateModel(d, m, y);
    }

//    public String calculateAge(){
//        LocalDate birthdate = new LocalDate (1970, 1, 20);
//        LocalDate now = new LocalDate();
//        Years age = Years.yearsBetween(birthdate, now);
//    }

    public interface DateTimeCallback {
        void callback(boolean result);
    }

//    private String getDate(long time) {
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(time);
//        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
//        return date;
//    }

    public static class DateModel {
        public int day, month, year;

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public DateModel(int day, int month, int year) {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public DateModel() {
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }
}
